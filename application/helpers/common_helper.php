<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('viewStatus'))
{
	function viewStatus($status_id)
	{
	if($status_id==1)
		return 'Active';
	else if($status_id==0)
		return 'Inactive';
	else
		return '';
	}
}