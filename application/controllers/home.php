<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends My_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('home_model');
	} 

    /*  Validation Rules */
    protected $validation_rules = array(
        'signup' => array(
            array(
                'field' => 'customer_name',
                'label' => 'Name',
                'rules' => 'trim|required|callback_alpha_dash_space'
            ),
            array(
                'field' => 'customer_email',
                'label' => 'Email',
                'rules' => 'trim|required|is_unique[tbl_customer.customer_email]|valid_email'
            ),
            array( 
                'field' => 'customer_password', 
                'label' => 'Password',   
                'rules' => 'trim|required'  
            ),
            array( 
                'field' => 'country_id', 
                'label' => 'Country',   
                'rules' => 'trim|required'  
            ),
            array( 
                'field' => 'state_id', 
                'label' => 'State',   
                'rules' => 'trim|required'  
            ),
            array( 
                'field' => 'city_id', 
                'label' => 'City',   
                'rules' => 'trim|required'  
            ),
            array( 
                'field' => 'customer_address', 
                'label' => 'Address',   
                'rules' => 'trim|required'  
            ),
            array( 
                'field' => 'customer_postal_code', 
                'label' => 'Postcode',   
                'rules' => 'trim|required'  
            )
        ),
        'login' => array(
            array(
                'field' => 'login_name',
                'label' => 'Email',
                'rules' => 'trim|required'
            ),
             array(
                'field' => 'customer_password',
                'label' => 'Password',
                'rules' => 'trim|required'
            )
        )
    );

	public function index() 
	{   
		$data['testimonial'] = $this->common_model->getData('tbl_testimonial', array('testimonial_status'=>'1'), 'multi', NULL, 'testimonial_id DESC', '10');
		$this->show_view_front('front/home', $data);
    }

    /* News Letter */
    public function newsletter()
    {
        $response = [];
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        if ($this->form_validation->run() !== false) {
            $email = $_POST['email'];
            $result = $this->common_model->getData('tbl_newsletter', array('newsletter_email' => $email), 'single');
            if(empty($result))
            {
                $data['newsletter_email'] = $email;
                $this->common_model->addData('tbl_newsletter', $data);
                $response = array(
                        'status' => 'success',
                        'msg' => $email.' is subscribed'
                    );
            }
            else
            {
                $response = array(
                        'status' => 'error',
                        'msg' => $email.' is already subscribed'
                    );
            }
        }
        else
        {
            $response = array(
                'status' => 'error',
                'msg' => 'Invalid email, try again'
                );
        }
        echo json_encode($response);
    }

    /* Login */    
    public function login() 
    {   
    	if($this->getSessionVal())
        {
            redirect(base_url().'home');
        }
        else
        {   
            if(isset($_POST['Submit']) && $_POST['Submit'] =='Submit')
            {
                $this->form_validation->set_rules($this->validation_rules['login']);
                if ($this->form_validation->run()) 
                {
                    $post['login_name'] = $_POST['login_name'];
                    $post['customer_password'] = md5($_POST['customer_password']);
                    $login_res = $this->home_model->checkUserLogin($post);
                    if(!empty($login_res))
                    {
                        $this->session->set_userdata('uemp', $login_res);
                        redirect(base_url().'home');
                    }
                    else
                    {
                        $msg = '<span class="text-danger">Invalid Username And Password</span>';
                        $this->session->set_flashdata('message', $msg);
                        redirect(base_url().'home/login');
                    }
                }
                else
                {           
                    $this->show_view_front('front/login');
                }
            }
            else
            {
                $this->show_view_front('front/login');
            }
        }
    }

    /* signup */    
    public function signup() 
    {   
        $this->show_view_front('front/signup');
    }

    /* signup data add */    
    public function signupData() 
    {   
        $post['customer_name'] = $this->input->post('customer_name');
        $post['login_name'] = $this->input->post('login_name');
        $post['customer_password'] = md5($this->input->post('customer_password'));
        $post['customer_email'] = $this->input->post('customer_email');
        $post['customer_address'] = $this->input->post('customer_address');
        $post['customer_postal_code'] = $this->input->post('customer_postal_code');
        $post['customer_city'] = $this->input->post('customer_city');
        $post['terms_conditions'] = $this->input->post('terms_conditions');
        $post['customer_created_date'] = date('Y-m-d');
        $post['customer_updated_date'] = date('Y-m-d');
        $post['customer_profile_img'] = 'webroot/upload/customers/customer.png';
        $n_post = $this->xssCleanValidate($post);
        $customer_id = $this->common_model->addData('tbl_customer', $n_post);
        if(!empty($customer_id)){
            $response = array( 
                'status' => 'success',
                'msg' => 'Signup successfully!!'
            );
        }
        else{
            $response = array(
                'status' => 'error',
                'msg' => 'Signup failed'
            );
        }
        echo json_encode($response);
    }

    /* check email */    
    public function checkEmailUsername() 
    {   
        $post['f_value'] = $this->input->post('f_value');
        $f_type = $this->input->post('f_type');
        if($f_type == 'Username'){
            $c_name = 'login_name';
        }
        else{
            $c_name = 'customer_email';
        }
        $result = $this->common_model->getData('tbl_customer', array($c_name=>$post['f_value']), 'single');
        if(!empty($result)){
            $response = array( 
                'status' => 'error',
                'msg' => 'This '.$f_type.' is already exist'
            );
        }
        else{
            $response = array(
                'status' => 'success',
                'msg' => ''
            );
        }
        echo json_encode($response);
    }
    
    /*  Logout */
    public function logout() 
    {   
        $this->session->sess_destroy();     
        redirect( base_url());
    }
    
    /* our_place */    
    public function our_place() 
    {   
        $this->show_view_front('front/our_place');
    }

    public function groundList()
    {
        $limit  = $this->input->post('limit');
        $offset = $this->input->post('offset');
        $game_id = $this->input->post('game_id');
        $play_members_id = $this->input->post('play_members_id');
        $services_id = $this->input->post('services_id');
        $offset1= ($limit*$offset);        
        $total_count = $this->home_model->CountGroundList($game_id,$play_members_id,$services_id);
        $data['ground_data'] = $this->home_model->getGroundList($limit,$offset1,$game_id,$play_members_id,$services_id);
        $data['total_count'] =count($total_count);
        $data['limit'] =$limit;
        $data['offset']=$offset;
        $ground_res = $this->load->view('front/ground_list', $data, true); 
        echo $ground_res;
    }

    public function gameDetail()
    {
        $ground_id  = $this->uri->segment(3);
        $ground_data = $this->common_model->getData('tbl_ground', array('ground_id'=>$ground_id), 'single');
        if(!empty($ground_data)){
            $data['ground_data'] = $ground_data;
            $this->show_view_front('front/ground_detail', $data);
        }
        else{
            redirect(base_url().'home/our_place');
        }
    }

    /* Partner */    
    public function partner() 
    {   
        $this->show_view_front('front/partner');
    }
    /* Our Vision */    
    public function ourVision() 
    {   
        $this->show_view_front('front/ourVision');
    }
    /* FAQ */    
    public function FAQ() 
    {   
        $this->show_view_front('front/FAQ');
    }
    /* Contact */    
    public function contact() 
    {   
        $this->show_view_front('front/contact');
    }
    /* Cookie Policy */    
    public function cookiePolicy() 
    {   
        $this->show_view_front('front/cookiePolicy');
    }

    /* Privacy Policy */    
    public function privacyPolicy() 
    {   
        $this->show_view_front('front/privacyPolicy');
    }
    
    /* Terms & Conditions */    
    public function termsConditions() 
    {   
        $this->show_view_front('front/termsConditions');
    }
    
    /* booking */    
    public function booking() 
    {   
        $this->show_view_front('front/booking');
    }

}