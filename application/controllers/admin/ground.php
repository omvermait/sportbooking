<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ground extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		if(!empty(MODULE_NAME))
		{
		   $this->load->model(MODULE_NAME.'ground_model');
		}   
	}
	
	/*	Validation Rules */
	protected $validation_rules = array
        (
        'groundAdd' => array(
            array(
                'field' => 'ground_name',
                'label' => 'Ground Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'ground_status',
                'label' => 'status',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'game_id',
                'label' => 'Game',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'ground_address',
                'label' => 'Address',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'facility_id[]',
                'label' => 'Facility',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'ground_description',
                'label' => 'Description',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'ground_size',
                'label' => 'Size',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'ground_price',
                'label' => 'Price',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'time_id[]',
                'label' => 'Available Time',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'center_id',
                'label' => 'Center',
                'rules' => 'trim|required'
            )
        ),
		'groundUpdate' => array(
            array(
                'field' => 'ground_name',
                'label' => 'Ground Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'ground_status',
                'label' => 'status',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'game_id',
                'label' => 'Game',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'ground_address',
                'label' => 'Address',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'facility_id[]',
                'label' => 'Facility',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'ground_description',
                'label' => 'Description',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'ground_size',
                'label' => 'Size',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'ground_price',
                'label' => 'Price',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'time_id[]',
                'label' => 'Available Time',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'center_id',
                'label' => 'Center',
                'rules' => 'trim|required'
            )
        )
    );

	/* Details */
	public function index()
	{
		if($this->checkViewPermission())
		{			
			$this->show_view(MODULE_NAME.'ground/ground_view', $this->data);
		}
		else
		{	
			redirect( base_url().MODULE_NAME.'dashboard/error/1');
		}
    } 

    public function loadData()
    {
    	$result = $this->ground_model->getAllDataList();
    	$data = array();
        $no = $_POST['start'];
        foreach ($result as $e_res) 
	    {
			$no++;
			$row   = array();
			$row[] = $no;
			$center_res = $this->common_model->getData('tbl_center', array('center_id'=>$e_res->center_id), 'single');
			$row[] = !empty($center_res) ? $center_res->center_name : '';
			$row[] = (!empty($e_res->ground_img)) ? '<img width="50px" src="'.base_url().''.$e_res->ground_img.'">' : '<img width="50px" src="'.base_url().'webroot/upload/dummy/dummy.png">';
			$row[] = $e_res->ground_name;
			$row[] = $e_res->ground_address;

			$game_res = $this->common_model->getData('tbl_game', array('game_id'=>$e_res->game_id), 'single');
			$row[] = (!empty($game_res)) ? $game_res->game_name : '';

			/*$quality_res = $this->common_model->getData('tbl_quality', array('quality_id'=>$e_res->quality_id), 'single');
			$row[] = (!empty($quality_res)) ? $quality_res->quality_name : '';*/

			$facility_arr = explode(',', $e_res->facility_id);
			$facility_name_str = '';
			if(!empty($facility_arr)){
				foreach($facility_arr as $facility_id) {
					$facility_res = $this->common_model->getData('tbl_facility', array('facility_id'=>$facility_id), 'single');
					$facility_name_str .= (!empty($facility_res)) ? $facility_res->facility_name.'<br>' : '';
				}
			}
			$row[] = $facility_name_str;


			/*$services_arr = explode(',', $e_res->services_id);
			$services_name_str = '';
			if(!empty($services_arr)){
				foreach($services_arr as $services_id) {
					$services_res = $this->common_model->getData('tbl_services', array('services_id'=>$services_id), 'single');
					$services_name_str .= (!empty($services_res)) ? $services_res->services_name.'<br>' : '';
				}
			}
			$row[] = $services_name_str;*/

			$row[] = viewStatus ($e_res->ground_status);
	 		$btn = '';
	 		if($this->checkViewPermission())
	 		{
	 			$btn .= '<a class="btn btn-success btn-sm" href="'.base_url().''.MODULE_NAME.'ground/groundView/'.$e_res->ground_id.'" title="View"><i class="fa fa-eye fa-1x "></i></a>&nbsp;&nbsp;';
	 		}
	 		if($this->checkEditPermission())
	 		{
	 			$btn .= '<a class="btn btn-success btn-sm" href="'.base_url().''.MODULE_NAME.'ground/addGround/'.$e_res->ground_id.'" title="Edit"><i class="fa fa-edit fa-1x "></i></a>&nbsp;&nbsp;';
	 		}
	 		if($this->checkDeletePermission())
	 		{
	 			$btn .= '<a class="confirm btn btn-danger btn-sm" onclick="return confirm(\'Are you sure you want to Delete\')" href="'.base_url().''.MODULE_NAME.'ground/deleteGround/'.$e_res->ground_id.'" title="Remove"><i class="fa fa-trash-o fa-1x" data-toggle="modal" data-target=".bs-example-modal-sm"></i></a>';
	 		}
	 		$row[] = $btn;
            $data[] = $row;
        }

        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => count($result),
			"recordsFiltered" => $this->ground_model->count_filtered(),
			"data" => $data,
		);
       	//output to json format
       	echo json_encode($output);
    }

    /* full  Details */
    public function groundView()
	{
		if($this->checkViewPermission())
		{			
			$ground_id = $this->uri->segment(4);
			$this->data['edit_ground'] = $this->common_model->getData('tbl_ground', array('ground_id'=>$ground_id), 'single');
			if(!empty($this->data['edit_ground']))
			{
				$this->show_view(MODULE_NAME.'ground/ground_full_view', $this->data);
			}	
			else
			{
				redirect(base_url().MODULE_NAME.'ground');
			}
		}
		else
		{	
			redirect( base_url().MODULE_NAME.'dashboard/error/1');
		}
    }

    /* Add & update */
    public function addGround()
    {
    	$ground_id = $this->uri->segment(4);
		if($ground_id)
		{
			if($this->checkEditPermission())
			{
				if (isset($_POST['Submit']) && $_POST['Submit'] == "Edit") 
				{                   
					$this->form_validation->set_rules($this->validation_rules['groundUpdate']);
					if($this->form_validation->run())
					{
						$post['center_id'] = $this->input->post('center_id');
                    	$post['ground_name'] = $this->input->post('ground_name');	
						$post['ground_status'] = $this->input->post('ground_status');
						// $post['quality_id'] = $this->input->post('quality_id');
						$post['game_id'] = $this->input->post('game_id');
						// $post['services_id'] = implode(',', $this->input->post('services_id'));
						$post['facility_id'] = implode(',', $this->input->post('facility_id'));
						$post['ground_size'] = $this->input->post('ground_size');
						$post['ground_price'] = $this->input->post('ground_price');
						$post['ground_address'] = $this->input->post('ground_address');
						$post['ground_description'] = $this->input->post('ground_description');
						$post['ground_updated_date'] = date('Y-m-d');

						if($_FILES["ground_img"]["name"]) 
						{
	                       	$ground_img = 'ground_img';
	                       	$fieldName = "ground_img";
	                       	$Path = 'webroot/upload/ground';
	                       	$ground_img = $this->ImageUpload($_FILES["ground_img"]["name"], $ground_img, $Path, $fieldName);
	                       	if(!empty($ground_img)){
	                       		$post['ground_img'] = $Path.'/'.$ground_img;
	                       	}
	                   	}
                        $n_post = $this->xssCleanValidate($post);
	                   	$this->common_model->updateData('tbl_ground', array('ground_id'=>$ground_id), $n_post); 

	                   	
						$n_post_t['ground_time_slot_status'] = '2';
						$this->common_model->updateData('tbl_ground_time_slot', array('ground_id'=>$ground_id), $n_post_t); 
	                   	$time_id = $this->input->post('time_id');
						if(!empty($time_id)){
							foreach($time_id as $t_val) {
								$time_slot_res = $this->common_model->getData('tbl_time_slot', array('time_id'=>$t_val), 'single');
								if(!empty($time_slot_res)){
									$post_t['ground_id'] = $ground_id;
									$post_t['time_id'] = $time_slot_res->time_id;
									$post_t['start_time'] = $time_slot_res->start_time;
									$post_t['end_time'] = $time_slot_res->end_time;
									$this->common_model->addData('tbl_ground_time_slot', $post_t);
								}
							}
						}

						if($_FILES["ground_img_m"]['name'])
						{
							$ground_img_m = $_FILES["ground_img_m"]['name'];
							for ($i=0; $i<count($ground_img_m); $i++) 
							{ 
								if(!empty($_FILES['ground_img_m']['name'][$i])){
									$_FILES['new_img_file']['name'] = $_FILES['ground_img_m']['name'][$i];
			        				$_FILES['new_img_file']['type'] = $_FILES['ground_img_m']['type'][$i];
					                $_FILES['new_img_file']['tmp_name'] = $_FILES['ground_img_m']['tmp_name'][$i];
					                $_FILES['new_img_file']['error'] = $_FILES['ground_img_m']['error'][$i];
					                $_FILES['new_img_file']['size'] = $_FILES['ground_img_m']['size'][$i];
					                $img_path = 'webroot/upload/ground/';
					                $img_fieldName = 'new_img_file';
					                $img_name = 'gallery_img';
					                $allowed_types = 'GIF | gif | JPE | jpe | JPEG | jpeg | JPG | jpg | PNG | png';
					                $imageName = $this->FileUpload($_FILES["new_img_file"]["name"], $img_name, $img_path, $img_fieldName, $allowed_types);
					                $img_name = (!empty($imageName)) ? $img_path.''.$imageName : 'upload/dummy/dummy.png';
					                $post_img['ground_img'] = $img_name;
		                            $post_img['ground_id'] =  $ground_id;
		                            $post_img['ground_img_status'] = '1';
		                            $this->common_model->addData('tbl_ground_img', $post_img);
		                        }
							}
						} 
	                   	$msg = 'Ground updated successfully!!';					
						$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
						redirect(base_url().MODULE_NAME.'ground');
					}
					else
					{
						$this->data['edit_ground'] = $this->common_model->getData('tbl_ground', array('ground_id'=>$ground_id), 'single');
						if(!empty($this->data['edit_ground']))
						{
							$this->show_view(MODULE_NAME.'ground/ground_update', $this->data);
						}	
						else
						{
							redirect(base_url().MODULE_NAME.'ground');
						}
					}
				}
				else
				{
					$this->data['edit_ground'] = $this->common_model->getData('tbl_ground', array('ground_id'=>$ground_id), 'single');
					if(!empty($this->data['edit_ground']))
					{
						$this->show_view(MODULE_NAME.'ground/ground_update', $this->data);
					}	
					else
					{
						redirect(base_url().MODULE_NAME.'ground');
					}
				}
			}
			else
			{	
				redirect( base_url().MODULE_NAME.'dashboard/error/1');
			}
		}
		else
		{
			if($this->checkAddPermission())
			{
				if (isset($_POST['Submit']) && $_POST['Submit'] == "Add") 
				{
					$this->form_validation->set_rules($this->validation_rules['groundAdd']);
					if($this->form_validation->run())
					{
						$post['center_id'] = $this->input->post('center_id');
                    	$post['ground_name'] = $this->input->post('ground_name');	
						$post['ground_status'] = $this->input->post('ground_status');
						// $post['quality_id'] = $this->input->post('quality_id');
						$post['game_id'] = $this->input->post('game_id');
						// $post['services_id'] = implode(',', $this->input->post('services_id'));
						$post['facility_id'] = implode(',', $this->input->post('facility_id'));
						$post['ground_size'] = $this->input->post('ground_size');
						$post['ground_price'] = $this->input->post('ground_price');
						$post['ground_address'] = $this->input->post('ground_address');
						$post['ground_description'] = $this->input->post('ground_description');
						$post['ground_created_date'] = date('Y-m-d');
						$post['ground_updated_date'] = date('Y-m-d');

						if($_FILES["ground_img"]["name"]) 
						{
	                       	$ground_img = 'ground_img';
	                       	$fieldName = "ground_img";
	                       	$Path = 'webroot/upload/ground';
	                       	$ground_img = $this->ImageUpload($_FILES["ground_img"]["name"], $ground_img, $Path, $fieldName);
	                       	if(!empty($ground_img)){
	                       		$post['ground_img'] = $Path.'/'.$ground_img;
	                       	}
	                   	}

                        $n_post = $this->xssCleanValidate($post);
						$ground_id = $this->common_model->addData('tbl_ground', $n_post);

						$time_id = $this->input->post('time_id');
						if(!empty($time_id)){
							foreach($time_id as $t_val) {
								$time_slot_res = $this->common_model->getData('tbl_time_slot', array('time_id'=>$t_val), 'single');
								if(!empty($time_slot_res)){
									$post_t['ground_id'] = $ground_id;
									$post_t['time_id'] = $time_slot_res->time_id;
									$post_t['start_time'] = $time_slot_res->start_time;
									$post_t['end_time'] = $time_slot_res->end_time;
									$this->common_model->addData('tbl_ground_time_slot', $post_t);
								}
							}
						}
						if($_FILES["ground_img_m"]['name'])
						{
							$ground_img_m = $_FILES["ground_img_m"]['name'];
							for ($i=0; $i<count($ground_img_m); $i++) 
							{ 
								$_FILES['new_img_file']['name'] = $_FILES['ground_img_m']['name'][$i];
		        				$_FILES['new_img_file']['type'] = $_FILES['ground_img_m']['type'][$i];
				                $_FILES['new_img_file']['tmp_name'] = $_FILES['ground_img_m']['tmp_name'][$i];
				                $_FILES['new_img_file']['error'] = $_FILES['ground_img_m']['error'][$i];
				                $_FILES['new_img_file']['size'] = $_FILES['ground_img_m']['size'][$i];
				                $img_path = 'webroot/upload/ground/';
				                $img_fieldName = 'new_img_file';
				                $img_name = 'gallery_img';
				                $allowed_types = 'GIF | gif | JPE | jpe | JPEG | jpeg | JPG | jpg | PNG | png';
				                $imageName = $this->FileUpload($_FILES["new_img_file"]["name"], $img_name, $img_path, $img_fieldName, $allowed_types);
				                $img_name = (!empty($imageName)) ? $img_path.''.$imageName : 'upload/dummy/dummy.png';
				                $post_img['ground_img'] = $img_name;
	                            $post_img['ground_id'] =  $ground_id;
	                            $post_img['ground_img_status'] = '1';
	                            $this->common_model->addData('tbl_ground_img', $post_img);

	                            if($i == '0'){
	                            	$post_i['ground_img'] = $img_name;
	                            	$this->common_model->updateData('tbl_ground', array('ground_id'=>$ground_id), $post_i);
	                            }
							}
						} 
	                   	$msg = 'Ground added successfully!!';					
						$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
						redirect(base_url().MODULE_NAME.'ground');
	                }
					else
					{
						$this->show_view(MODULE_NAME.'ground/ground_add', $this->data);
					}
				}
				else
				{
					$this->show_view(MODULE_NAME.'ground/ground_add', $this->data);
				}
			}
			else
			{	
				redirect( base_url().MODULE_NAME.'dashboard/error/1');
			}
		}
    }

    /* Delete */
	public function deleteGround()
	{
		if($this->checkDeletePermission())
		{
			$ground_id = $this->uri->segment(4);
			$n_post['ground_status'] = '2';
			$this->common_model->updateData('tbl_ground', array('ground_id'=>$ground_id), $n_post); 
			$n_post_t['ground_time_slot_status'] = '2';
			$this->common_model->updateData('tbl_ground_time_slot', array('ground_id'=>$ground_id), $n_post_t); 
			$msg = 'Ground remove successfully...!';					
			$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
			redirect(base_url().MODULE_NAME.'ground');
		}
	}

	public function removeGroundImage()
	{
		$ground_img_id = $this->input->post('ground_img_id');
		$this->common_model->deleteData('tbl_ground_img', array('ground_img_id'=>$ground_img_id)); 
		echo "1";
	}
	
}

/* End of file */?>