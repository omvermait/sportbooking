<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Services extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		if(!empty(MODULE_NAME))
		{
		   $this->load->model(MODULE_NAME.'services_model');
		}   
	}
	
	/*	Validation Rules */
	protected $validation_rules = array
        (
        'servicesAdd' => array(
            array(
                'field' => 'services_name',
                'label' => 'Services Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'services_status',
                'label' => 'Services status',
                'rules' => 'trim|required'
            )/*,
            array(
                'field' => 'center_id',
                'label' => 'Center',
                'rules' => 'trim|required'
            )  */ 
        ),
		'servicesUpdate' => array(
            array(
                'field' => 'services_name',
                'label' => 'Services Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'services_status',
                'label' => 'Services status',
                'rules' => 'trim|required'
            )/*,
            array(
                'field' => 'center_id',
                'label' => 'Center',
                'rules' => 'trim|required'
            )*/ 
        )
    );

	/* Details */
	public function index()
	{
		if($this->checkViewPermission())
		{			
			$this->show_view(MODULE_NAME.'services/services_view', $this->data);
		}
		else
		{	
			redirect( base_url().MODULE_NAME.'dashboard/error/1');
		}
    } 

    public function loadData()
    {
    	$result = $this->services_model->getAllDataList();
    	$data = array();
        $no = $_POST['start'];
        foreach ($result as $e_res) 
	    {
			$no++;
			$row   = array();
			$row[] = $no;
			/*$center_id = substr($e_res->center_id, 2, 10);
			$center_res = $this->common_model->getTableValue('tbl_center', 'center_id', $center_id);
			$row[] = !empty($center_res) ? $center_res->center_name : '';*/
			$row[] = (!empty($e_res->services_img)) ? '<img width="50px" src="'.base_url().''.$e_res->services_img.'">' : '<img width="50px" src="'.base_url().'webroot/upload/dummy/dummy.png">';
			$row[] = $e_res->services_name;
			$row[] = viewStatus ($e_res->services_status);
	 		$btn = '';
	 		if($this->checkViewPermission())
	 		{
	 			$btn .= '<a class="btn btn-success btn-sm" href="'.base_url().''.MODULE_NAME.'services/servicesView/'.$e_res->services_id.'" title="View"><i class="fa fa-eye fa-1x "></i></a>&nbsp;&nbsp;';
	 		}
	 		if($this->checkEditPermission())
	 		{
	 			$btn .= '<a class="btn btn-success btn-sm" href="'.base_url().''.MODULE_NAME.'services/addServices/'.$e_res->services_id.'" title="Edit"><i class="fa fa-edit fa-1x "></i></a>&nbsp;&nbsp;';
	 		}
	 		if($this->checkDeletePermission())
	 		{
	 			$btn .= '<a class="confirm btn btn-danger btn-sm" onclick="return confirm(\'Are you sure you want to Delete\')" href="'.base_url().''.MODULE_NAME.'services/deleteServices/'.$e_res->services_id.'" title="Remove"><i class="fa fa-trash-o fa-1x" data-toggle="modal" data-target=".bs-example-modal-sm"></i></a>';
	 		}
	 		$row[] = $btn;
            $data[] = $row;
        }

        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => count($result),
			"recordsFiltered" => $this->services_model->count_filtered(),
			"data" => $data,
		);
       	//output to json format
       	echo json_encode($output);
    }
    /* full  Details */
    public function servicesView()
	{
		if($this->checkViewPermission())
		{			
			$services_id = $this->uri->segment(4);
			$this->data['edit_services'] = $this->common_model->getData('tbl_services', array('services_id'=>$services_id), 'single');
			if(!empty($this->data['edit_services']))
			{
				$this->show_view(MODULE_NAME.'services/services_full_view', $this->data);
			}	
			else
			{
				redirect(base_url().MODULE_NAME.'services');
			}
		}
		else
		{	
			redirect( base_url().MODULE_NAME.'dashboard/error/1');
		}
    }

    /* Add & update */
    public function addServices()
    {
    	$services_id = $this->uri->segment(4);
		if($services_id)
		{
			if($this->checkEditPermission())
			{
				if (isset($_POST['Submit']) && $_POST['Submit'] == "Edit") 
				{                   
					$this->form_validation->set_rules($this->validation_rules['servicesUpdate']);
					if($this->form_validation->run())
					{
						// $post['center_id'] = '0,'.$this->input->post('center_id');
                    	$post['services_name'] = $this->input->post('services_name');	
						$post['services_status'] = $this->input->post('services_status');

						if($_FILES["services_img"]["name"]) 
						{
	                       $services_img = 'services_img';
	                       $fieldName = "services_img";
	                       $Path = 'webroot/upload/services';
	                       $services_img = $this->ImageUpload($_FILES["services_img"]["name"], $services_img, $Path, $fieldName);
	                       $post['services_img'] = $Path.'/'.$services_img;
	                   	}

						$post['services_created_date'] = date('Y-m-d');
						$post['services_updated_date'] = date('Y-m-d');
                        $n_post = $this->xssCleanValidate($post);
	                   	$this->common_model->updateData('tbl_services', array('services_id'=>$services_id), $n_post); 
	                   	$msg = 'Services updated successfully!!';					
						$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
						redirect(base_url().MODULE_NAME.'services');
					}
					else
					{
						$this->data['edit_services'] = $this->common_model->getData('tbl_services', array('services_id'=>$services_id), 'single');
						if(!empty($this->data['edit_services']))
						{
							$this->show_view(MODULE_NAME.'services/services_update', $this->data);
						}	
						else
						{
							redirect(base_url().MODULE_NAME.'services');
						}
					}
				}
				else
				{
					$this->data['edit_services'] = $this->common_model->getData('tbl_services', array('services_id'=>$services_id), 'single');
					if(!empty($this->data['edit_services']))
					{
						$this->show_view(MODULE_NAME.'services/services_update', $this->data);
					}	
					else
					{
						redirect(base_url().MODULE_NAME.'services');
					}
				}
			}
			else
			{	
				redirect( base_url().MODULE_NAME.'dashboard/error/1');
			}
		}
		else
		{
			if($this->checkAddPermission())
			{
				if (isset($_POST['Submit']) && $_POST['Submit'] == "Add") 
				{
					$this->form_validation->set_rules($this->validation_rules['servicesAdd']);
					if($this->form_validation->run())
					{
						// $post['center_id'] = '0,'.$this->input->post('center_id');
                    	$post['services_name'] = $this->input->post('services_name');	
						$post['services_status'] = $this->input->post('services_status');

						if($_FILES["services_img"]["name"]) 
						{
	                       $services_img = 'services_img';
	                       $fieldName = "services_img";
	                       $Path = 'webroot/upload/services';
	                       $services_img = $this->ImageUpload($_FILES["services_img"]["name"], $services_img, $Path, $fieldName);
	                       $services_img = $Path.'/'.$services_img;
	                   	}
	                   	else{
	                       $services_img = base_url().'upload/dummy/dummy.png';
	                   	}
	                    $post['services_img'] = $services_img;

						$post['services_created_date'] = date('Y-m-d');
						$post['services_updated_date'] = date('Y-m-d');
                        $n_post = $this->xssCleanValidate($post);
						$this->common_model->addData('tbl_services', $n_post);
	                   	$msg = 'Services added successfully!!';					
						$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
						redirect(base_url().MODULE_NAME.'services');
	                }
					else
					{
						$this->show_view(MODULE_NAME.'services/services_add', $this->data);
					}
				}
				else
				{
					$this->show_view(MODULE_NAME.'services/services_add', $this->data);
				}
			}
			else
			{	
				redirect( base_url().MODULE_NAME.'dashboard/error/1');
			}
		}
    }

    /* Delete */
	public function deleteServices()
	{
		if($this->checkDeletePermission())
		{
			$services_id = $this->uri->segment(4);
			$n_post['services_status'] = '2';
			$this->common_model->updateData('tbl_services', array('services_id'=>$services_id), $n_post); 
			$msg = 'Services remove successfully...!';					
			$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
			redirect(base_url().MODULE_NAME.'services');
		}
	}
	
}

/* End of file */?>