<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Facility extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		if(!empty(MODULE_NAME))
		{
		   $this->load->model(MODULE_NAME.'facility_model');
		}   
	}
	
	/*	Validation Rules */
	protected $validation_rules = array
        (
        'facilityAdd' => array(
            array(
                'field' => 'facility_name',
                'label' => 'Facility Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'facility_status',
                'label' => 'Facility status',
                'rules' => 'trim|required'
            )  
        ),
		'facilityUpdate' => array(
            array(
                'field' => 'facility_name',
                'label' => 'Facility Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'facility_status',
                'label' => 'Facility status',
                'rules' => 'trim|required'
            )
        )
    );

	/* Details */
	public function index()
	{
		if($this->checkViewPermission())
		{			
			$this->show_view(MODULE_NAME.'facility/facility_view', $this->data);
		}
		else
		{	
			redirect( base_url().MODULE_NAME.'dashboard/error/1');
		}
    } 

    public function loadData()
    {
    	$result = $this->facility_model->getAllDataList();
    	$data = array();
        $no = $_POST['start'];
        foreach ($result as $e_res) 
	    {
			$no++;
			$row   = array();
			$row[] = $no;
			$row[] = (!empty($e_res->facility_img)) ? '<img width="50px" src="'.base_url().''.$e_res->facility_img.'">' : '<img width="50px" src="'.base_url().'webroot/upload/dummy/dummy.png">';
			$row[] = $e_res->facility_name;
			$row[] = viewStatus ($e_res->facility_status);
	 		$btn = '';
	 		if($this->checkViewPermission())
	 		{
	 			$btn .= '<a class="btn btn-success btn-sm" href="'.base_url().''.MODULE_NAME.'facility/facilityView/'.$e_res->facility_id.'" title="View"><i class="fa fa-eye fa-1x "></i></a>&nbsp;&nbsp;';
	 		}
	 		if($this->checkEditPermission())
	 		{
	 			$btn .= '<a class="btn btn-success btn-sm" href="'.base_url().''.MODULE_NAME.'facility/addFacility/'.$e_res->facility_id.'" title="Edit"><i class="fa fa-edit fa-1x "></i></a>&nbsp;&nbsp;';
	 		}
	 		if($this->checkDeletePermission())
	 		{
	 			$btn .= '<a class="confirm btn btn-danger btn-sm" onclick="return confirm(\'Are you sure you want to Delete\')" href="'.base_url().''.MODULE_NAME.'facility/deleteFacility/'.$e_res->facility_id.'" title="Remove"><i class="fa fa-trash-o fa-1x" data-toggle="modal" data-target=".bs-example-modal-sm"></i></a>';
	 		}
	 		$row[] = $btn;
            $data[] = $row;
        }

        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => count($result),
			"recordsFiltered" => $this->facility_model->count_filtered(),
			"data" => $data,
		);
       	//output to json format
       	echo json_encode($output);
    }
    /* full  Details */
    public function facilityView()
	{
		if($this->checkViewPermission())
		{			
			$facility_id = $this->uri->segment(4);
			$this->data['edit_facility'] = $this->common_model->getData('tbl_facility', array('facility_id'=>$facility_id), 'single');
			if(!empty($this->data['edit_facility']))
			{
				$this->show_view(MODULE_NAME.'facility/facility_full_view', $this->data);
			}	
			else
			{
				redirect(base_url().MODULE_NAME.'facility');
			}
		}
		else
		{	
			redirect( base_url().MODULE_NAME.'dashboard/error/1');
		}
    }

    /* Add & update */
    public function addFacility()
    {
    	$facility_id = $this->uri->segment(4);
		if($facility_id)
		{
			if($this->checkEditPermission())
			{
				if (isset($_POST['Submit']) && $_POST['Submit'] == "Edit") 
				{                   
					$this->form_validation->set_rules($this->validation_rules['facilityUpdate']);
					if($this->form_validation->run())
					{
                    	$post['facility_name'] = $this->input->post('facility_name');	
						$post['facility_status'] = $this->input->post('facility_status');

						if($_FILES["facility_img"]["name"]) 
						{
	                       $facility_img = 'facility_img';
	                       $fieldName = "facility_img";
	                       $Path = 'webroot/upload/facility';
	                       $facility_img = $this->ImageUpload($_FILES["facility_img"]["name"], $facility_img, $Path, $fieldName);
	                       $post['facility_img'] = $Path.'/'.$facility_img;
	                   	}

						$post['facility_created_date'] = date('Y-m-d');
						$post['facility_updated_date'] = date('Y-m-d');
                        $n_post = $this->xssCleanValidate($post);
	                   	$this->common_model->updateData('tbl_facility', array('facility_id'=>$facility_id), $n_post); 
	                   	$msg = 'Facility updated successfully!!';					
						$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
						redirect(base_url().MODULE_NAME.'facility');
					}
					else
					{
						$this->data['edit_facility'] = $this->common_model->getData('tbl_facility', array('facility_id'=>$facility_id), 'single');
						if(!empty($this->data['edit_facility']))
						{
							$this->show_view(MODULE_NAME.'facility/facility_update', $this->data);
						}	
						else
						{
							redirect(base_url().MODULE_NAME.'facility');
						}
					}
				}
				else
				{
					$this->data['edit_facility'] = $this->common_model->getData('tbl_facility', array('facility_id'=>$facility_id), 'single');
					if(!empty($this->data['edit_facility']))
					{
						$this->show_view(MODULE_NAME.'facility/facility_update', $this->data);
					}	
					else
					{
						redirect(base_url().MODULE_NAME.'facility');
					}
				}
			}
			else
			{	
				redirect( base_url().MODULE_NAME.'dashboard/error/1');
			}
		}
		else
		{
			if($this->checkAddPermission())
			{
				if (isset($_POST['Submit']) && $_POST['Submit'] == "Add") 
				{
					$this->form_validation->set_rules($this->validation_rules['facilityAdd']);
					if($this->form_validation->run())
					{
                    	$post['facility_name'] = $this->input->post('facility_name');	
						$post['facility_status'] = $this->input->post('facility_status');

						if($_FILES["facility_img"]["name"]) 
						{
	                       $facility_img = 'facility_img';
	                       $fieldName = "facility_img";
	                       $Path = 'webroot/upload/facility';
	                       $facility_img = $this->ImageUpload($_FILES["facility_img"]["name"], $facility_img, $Path, $fieldName);
	                       $facility_img = $Path.'/'.$facility_img;
	                   	}
	                   	else{
	                       $facility_img = 'webroot/upload/dummy/dummy.png';
	                   	}
	                    $post['facility_img'] = $facility_img;

						$post['facility_created_date'] = date('Y-m-d');
						$post['facility_updated_date'] = date('Y-m-d');
                        $n_post = $this->xssCleanValidate($post);
						$this->common_model->addData('tbl_facility', $n_post);
	                   	$msg = 'Facility added successfully!!';					
						$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
						redirect(base_url().MODULE_NAME.'facility');
	                }
					else
					{
						$this->show_view(MODULE_NAME.'facility/facility_add', $this->data);
					}
				}
				else
				{
					$this->show_view(MODULE_NAME.'facility/facility_add', $this->data);
				}
			}
			else
			{	
				redirect( base_url().MODULE_NAME.'dashboard/error/1');
			}
		}
    }

    /* Delete */
	public function deleteFacility()
	{
		if($this->checkDeletePermission())
		{
			$facility_id = $this->uri->segment(4);
			$n_post['facility_status'] = '2';
			$this->common_model->updateData('tbl_facility', array('facility_id'=>$facility_id), $n_post); 
			$msg = 'Facility remove successfully...!';					
			$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
			redirect(base_url().MODULE_NAME.'facility');
		}
	}
	
}

/* End of file */?>