<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class PrivacyPolicy extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
	}
	
	/*	Validation Rules */
	protected $validation_rules = array
        (
		'privacyPolicyUpdate' => array(
            array(
                'field' => 'privacyPolicy_description',
                'label' => 'Privacy Policy',
                'rules' => 'trim|required'
            ) 
        )
    );

	/* Details */
	public function index()
	{
		if($this->checkViewPermission() || $this->checkAddPermission() || $this->checkEditPermission())
		{	
			$privacyPolicy_id = '1';
			if (isset($_POST['Submit']) && $_POST['Submit'] == "Edit") 
			{                   
				$this->form_validation->set_rules($this->validation_rules['privacyPolicyUpdate']);
				if($this->form_validation->run())
				{
                	$post['privacyPolicy_description'] = $this->input->post('privacyPolicy_description');	
					$post['privacyPolicy_updated_date'] = date('Y-m-d');
                    $n_post = $this->xssCleanValidate($post);
                   	$this->common_model->updateData('tbl_privacypolicy', array('privacyPolicy_id'=>$privacyPolicy_id), $n_post); 
                   	$msg = 'Privacy policy updated successfully!!';					
					$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
					redirect(base_url().MODULE_NAME.'privacyPolicy');
				}
				else
				{
					$this->data['edit_privacyPolicy'] = $this->common_model->getData('tbl_privacypolicy', array('privacyPolicy_id'=>$privacyPolicy_id), 'single');
					if(!empty($this->data['edit_privacyPolicy']))
					{
						$this->show_view(MODULE_NAME.'privacyPolicy/privacyPolicy_view', $this->data);
					}	
					else
					{
						redirect(base_url().MODULE_NAME.'privacyPolicy');
					}
				}
			}
			else
			{
				$this->data['edit_privacyPolicy'] = $this->common_model->getData('tbl_privacypolicy', array('privacyPolicy_id'=>$privacyPolicy_id), 'single');
				if(!empty($this->data['edit_privacyPolicy']))
				{
					$this->show_view(MODULE_NAME.'privacyPolicy/privacyPolicy_view', $this->data);
				}	
				else
				{
					redirect(base_url().MODULE_NAME.'privacyPolicy');
				}
			}
		}
		else
		{	
			redirect( base_url().MODULE_NAME.'dashboard/error/1');
		}
    } 	
}

/* End of file */?>