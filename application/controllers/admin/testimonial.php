<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Testimonial extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		if(!empty(MODULE_NAME))
		{
		   $this->load->model(MODULE_NAME.'testimonial_model');
		}   
	}
	
	/*	Validation Rules */
	protected $validation_rules = array
        (
        'testimonialAdd' => array(
            array(
                'field' => 'testimonial_name',
                'label' => 'Testimonial Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'testimonial_title',
                'label' => 'Testimonial Title',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'testimonial_description',
                'label' => 'Testimonial Description',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'testimonial_status',
                'label' => 'Testimonial status',
                'rules' => 'trim|required'
            )  
        ),
		'testimonialUpdate' => array(
            array(
                'field' => 'testimonial_name',
                'label' => 'Testimonial Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'testimonial_title',
                'label' => 'Testimonial Title',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'testimonial_description',
                'label' => 'Testimonial Description',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'testimonial_status',
                'label' => 'Testimonial status',
                'rules' => 'trim|required'
            )
        )
    );

	/* Details */
	public function index()
	{
		if($this->checkViewPermission())
		{			
			$this->show_view(MODULE_NAME.'testimonial/testimonial_view', $this->data);
		}
		else
		{	
			redirect( base_url().MODULE_NAME.'dashboard/error/1');
		}
    } 

    public function loadData()
    {
    	$result = $this->testimonial_model->getAllDataList();
    	$data = array();
        $no = $_POST['start'];
        foreach ($result as $e_res) 
	    {
			$no++;
			$row   = array();
			$row[] = $no;
			$row[] = (!empty($e_res->testimonial_img)) ? '<img width="50px" src="'.base_url().''.$e_res->testimonial_img.'">' : '<img width="50px" src="'.base_url().'webroot/upload/dummy/user.png">';
			$row[] = $e_res->testimonial_name;
			$row[] = $e_res->testimonial_title;
			$row[] = viewStatus ($e_res->testimonial_status);
	 		$btn = '';
	 		if($this->checkViewPermission())
	 		{
	 			$btn .= '<a class="btn btn-success btn-sm" href="'.base_url().''.MODULE_NAME.'testimonial/testimonialView/'.$e_res->testimonial_id.'" title="View"><i class="fa fa-eye fa-1x "></i></a>&nbsp;&nbsp;';
	 		}
	 		if($this->checkEditPermission())
	 		{
	 			$btn .= '<a class="btn btn-success btn-sm" href="'.base_url().''.MODULE_NAME.'testimonial/addTestimonial/'.$e_res->testimonial_id.'" title="Edit"><i class="fa fa-edit fa-1x "></i></a>&nbsp;&nbsp;';
	 		}
	 		if($this->checkDeletePermission())
	 		{
	 			$btn .= '<a class="confirm btn btn-danger btn-sm" onclick="return confirm(\'Are you sure you want to Delete\')" href="'.base_url().''.MODULE_NAME.'testimonial/deleteTestimonial/'.$e_res->testimonial_id.'" title="Remove"><i class="fa fa-trash-o fa-1x" data-toggle="modal" data-target=".bs-example-modal-sm"></i></a>';
	 		}
	 		$row[] = $btn;
            $data[] = $row;
        }

        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => count($result),
			"recordsFiltered" => $this->testimonial_model->count_filtered(),
			"data" => $data,
		);
       	//output to json format
       	echo json_encode($output);
    }
    /* full  Details */
    public function testimonialView()
	{
		if($this->checkViewPermission())
		{			
			$testimonial_id = $this->uri->segment(4);
			$this->data['edit_testimonial'] = $this->common_model->getData('tbl_testimonial', array('testimonial_id'=>$testimonial_id), 'single');
			if(!empty($this->data['edit_testimonial']))
			{
				$this->show_view(MODULE_NAME.'testimonial/testimonial_full_view', $this->data);
			}	
			else
			{
				redirect(base_url().MODULE_NAME.'testimonial');
			}
		}
		else
		{	
			redirect( base_url().MODULE_NAME.'dashboard/error/1');
		}
    }

    /* Add & update */
    public function addTestimonial()
    {
    	$testimonial_id = $this->uri->segment(4);
		if($testimonial_id)
		{
			if($this->checkEditPermission())
			{
				if (isset($_POST['Submit']) && $_POST['Submit'] == "Edit") 
				{                   
					$this->form_validation->set_rules($this->validation_rules['testimonialUpdate']);
					if($this->form_validation->run())
					{
                    	$post['testimonial_title'] = $this->input->post('testimonial_title');	
                    	$post['testimonial_description'] = $this->input->post('testimonial_description');	
                    	$post['testimonial_name'] = $this->input->post('testimonial_name');	
						$post['testimonial_status'] = $this->input->post('testimonial_status');

						if($_FILES["testimonial_img"]["name"]) 
						{
	                       $testimonial_img = 'testimonial_img';
	                       $fieldName = "testimonial_img";
	                       $Path = 'webroot/upload/testimonial';
	                       $testimonial_img = $this->ImageUpload($_FILES["testimonial_img"]["name"], $testimonial_img, $Path, $fieldName);
	                       $post['testimonial_img'] = $Path.'/'.$testimonial_img;
	                   	}

						$post['testimonial_created_date'] = date('Y-m-d');
						$post['testimonial_updated_date'] = date('Y-m-d');
                        $n_post = $this->xssCleanValidate($post);
	                   	$this->common_model->updateData('tbl_testimonial', array('testimonial_id'=>$testimonial_id), $n_post); 
	                   	$msg = 'Testimonial updated successfully!!';					
						$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
						redirect(base_url().MODULE_NAME.'testimonial');
					}
					else
					{
						$this->data['edit_testimonial'] = $this->common_model->getData('tbl_testimonial', array('testimonial_id'=>$testimonial_id), 'single');
						if(!empty($this->data['edit_testimonial']))
						{
							$this->show_view(MODULE_NAME.'testimonial/testimonial_update', $this->data);
						}	
						else
						{
							redirect(base_url().MODULE_NAME.'testimonial');
						}
					}
				}
				else
				{
					$this->data['edit_testimonial'] = $this->common_model->getData('tbl_testimonial', array('testimonial_id'=>$testimonial_id), 'single');
					if(!empty($this->data['edit_testimonial']))
					{
						$this->show_view(MODULE_NAME.'testimonial/testimonial_update', $this->data);
					}	
					else
					{
						redirect(base_url().MODULE_NAME.'testimonial');
					}
				}
			}
			else
			{	
				redirect( base_url().MODULE_NAME.'dashboard/error/1');
			}
		}
		else
		{
			if($this->checkAddPermission())
			{
				if (isset($_POST['Submit']) && $_POST['Submit'] == "Add") 
				{
					$this->form_validation->set_rules($this->validation_rules['testimonialAdd']);
					if($this->form_validation->run())
					{
						$post['testimonial_title'] = $this->input->post('testimonial_title');	
                    	$post['testimonial_description'] = $this->input->post('testimonial_description');	
                    	$post['testimonial_name'] = $this->input->post('testimonial_name');	
						$post['testimonial_status'] = $this->input->post('testimonial_status');

						if($_FILES["testimonial_img"]["name"]) 
						{
	                       $testimonial_img = 'testimonial_img';
	                       $fieldName = "testimonial_img";
	                       $Path = 'webroot/upload/testimonial';
	                       $testimonial_img = $this->ImageUpload($_FILES["testimonial_img"]["name"], $testimonial_img, $Path, $fieldName);
	                       $post['testimonial_img'] = $Path.'/'.$testimonial_img;
	                   	}

						$post['testimonial_created_date'] = date('Y-m-d');
						$post['testimonial_updated_date'] = date('Y-m-d');
                        $n_post = $this->xssCleanValidate($post);
						$this->common_model->addData('tbl_testimonial', $n_post);
	                   	$msg = 'Testimonial added successfully!!';					
						$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
						redirect(base_url().MODULE_NAME.'testimonial');
	                }
					else
					{
						$this->show_view(MODULE_NAME.'testimonial/testimonial_add', $this->data);
					}
				}
				else
				{
					$this->show_view(MODULE_NAME.'testimonial/testimonial_add', $this->data);
				}
			}
			else
			{	
				redirect( base_url().MODULE_NAME.'dashboard/error/1');
			}
		}
    }

    /* Delete */
	public function deleteTestimonial()
	{
		if($this->checkDeletePermission())
		{
			$testimonial_id = $this->uri->segment(4);
			$n_post['testimonial_status'] = '2';
			$this->common_model->updateData('tbl_testimonial', array('testimonial_id'=>$testimonial_id), $n_post); 
			$msg = 'Testimonial remove successfully...!';					
			$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
			redirect(base_url().MODULE_NAME.'testimonial');
		}
	}
	
}

/* End of file */?>