<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class OurVision extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
	}
	
	/*	Validation Rules */
	protected $validation_rules = array
        (
		'ourVisionUpdate' => array(
            array(
                'field' => 'ourVision_description',
                'label' => 'Our Vision',
                'rules' => 'trim|required'
            ) 
        )
    );

	/* Details */
	public function index()
	{
		if($this->checkViewPermission() || $this->checkAddPermission() || $this->checkEditPermission())
		{	
			$ourVision_id = '1';
			if (isset($_POST['Submit']) && $_POST['Submit'] == "Edit") 
			{                   
				$this->form_validation->set_rules($this->validation_rules['ourVisionUpdate']);
				if($this->form_validation->run())
				{
                	$post['ourVision_description'] = $this->input->post('ourVision_description');	
					$post['ourVision_updated_date'] = date('Y-m-d');
                    $n_post = $this->xssCleanValidate($post);
                   	$this->common_model->updateData('tbl_ourvision', array('ourVision_id'=>$ourVision_id), $n_post); 
                   	$msg = 'Our Vision updated successfully!!';					
					$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
					redirect(base_url().MODULE_NAME.'ourVision');
				}
				else
				{
					$this->data['edit_ourVision'] = $this->common_model->getData('tbl_ourvision', array('ourVision_id'=>$ourVision_id), 'single');
					if(!empty($this->data['edit_ourVision']))
					{
						$this->show_view(MODULE_NAME.'ourVision/ourVision_view', $this->data);
					}	
					else
					{
						redirect(base_url().MODULE_NAME.'ourVision');
					}
				}
			}
			else
			{
				$this->data['edit_ourVision'] = $this->common_model->getData('tbl_ourvision', array('ourVision_id'=>$ourVision_id), 'single');
				if(!empty($this->data['edit_ourVision']))
				{
					$this->show_view(MODULE_NAME.'ourVision/ourVision_view', $this->data);
				}	
				else
				{
					redirect(base_url().MODULE_NAME.'ourVision');
				}
			}
		}
		else
		{	
			redirect( base_url().MODULE_NAME.'dashboard/error/1');
		}
    } 	
}

/* End of file */?>