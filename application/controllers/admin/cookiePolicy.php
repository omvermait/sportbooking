<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CookiePolicy extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
	}
	
	/*	Validation Rules */
	protected $validation_rules = array
        (
		'cookiePolicyUpdate' => array(
            array(
                'field' => 'cookiePolicy_description',
                'label' => 'Cookie Policy',
                'rules' => 'trim|required'
            ) 
        )
    );

	/* Details */
	public function index()
	{
		if($this->checkViewPermission() || $this->checkAddPermission() || $this->checkEditPermission())
		{	
			$cookiePolicy_id = '1';
			if (isset($_POST['Submit']) && $_POST['Submit'] == "Edit") 
			{                   
				$this->form_validation->set_rules($this->validation_rules['cookiePolicyUpdate']);
				if($this->form_validation->run())
				{
                	$post['cookiePolicy_description'] = $this->input->post('cookiePolicy_description');	
					$post['cookiePolicy_updated_date'] = date('Y-m-d');
                    $n_post = $this->xssCleanValidate($post);
                   	$this->common_model->updateData('tbl_cookiepolicy', array('cookiePolicy_id'=>$cookiePolicy_id), $n_post); 
                   	$msg = 'Cookie policy updated successfully!!';					
					$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
					redirect(base_url().MODULE_NAME.'cookiePolicy');
				}
				else
				{
					$this->data['edit_cookiePolicy'] = $this->common_model->getData('tbl_cookiepolicy', array('cookiePolicy_id'=>$cookiePolicy_id), 'single');
					if(!empty($this->data['edit_cookiePolicy']))
					{
						$this->show_view(MODULE_NAME.'cookiePolicy/cookiePolicy_view', $this->data);
					}	
					else
					{
						redirect(base_url().MODULE_NAME.'cookiePolicy');
					}
				}
			}
			else
			{
				$this->data['edit_cookiePolicy'] = $this->common_model->getData('tbl_cookiepolicy', array('cookiePolicy_id'=>$cookiePolicy_id), 'single');
				if(!empty($this->data['edit_cookiePolicy']))
				{
					$this->show_view(MODULE_NAME.'cookiePolicy/cookiePolicy_view', $this->data);
				}	
				else
				{
					redirect(base_url().MODULE_NAME.'cookiePolicy');
				}
			}
		}
		else
		{	
			redirect( base_url().MODULE_NAME.'dashboard/error/1');
		}
    } 	
}

/* End of file */?>