<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class TermsConditions extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
	}
	
	/*	Validation Rules */
	protected $validation_rules = array
        (
		'termsConditionsUpdate' => array(
            array(
                'field' => 'termsConditions_description',
                'label' => 'Terms & Conditions',
                'rules' => 'trim|required'
            ) 
        )
    );

	/* Details */
	public function index()
	{
		if($this->checkViewPermission() || $this->checkAddPermission() || $this->checkEditPermission())
		{	
			$termsConditions_id = '1';
			if (isset($_POST['Submit']) && $_POST['Submit'] == "Edit") 
			{                   
				$this->form_validation->set_rules($this->validation_rules['termsConditionsUpdate']);
				if($this->form_validation->run())
				{
                	$post['termsConditions_description'] = $this->input->post('termsConditions_description');	
					$post['termsConditions_updated_date'] = date('Y-m-d');
                    $n_post = $this->xssCleanValidate($post);
                   	$this->common_model->updateData('tbl_termsconditions', array('termsConditions_id'=>$termsConditions_id), $n_post); 
                   	$msg = 'Terms & Conditions updated successfully!!';					
					$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
					redirect(base_url().MODULE_NAME.'termsConditions');
				}
				else
				{
					$this->data['edit_termsConditions'] = $this->common_model->getData('tbl_termsconditions', array('termsConditions_id'=>$termsConditions_id), 'single');
					if(!empty($this->data['edit_termsConditions']))
					{
						$this->show_view(MODULE_NAME.'termsConditions/termsConditions_view', $this->data);
					}	
					else
					{
						redirect(base_url().MODULE_NAME.'termsConditions');
					}
				}
			}
			else
			{
				$this->data['edit_termsConditions'] = $this->common_model->getData('tbl_termsconditions', array('termsConditions_id'=>$termsConditions_id), 'single');
				if(!empty($this->data['edit_termsConditions']))
				{
					$this->show_view(MODULE_NAME.'termsConditions/termsConditions_view', $this->data);
				}	
				else
				{
					redirect(base_url().MODULE_NAME.'termsConditions');
				}
			}
		}
		else
		{	
			redirect( base_url().MODULE_NAME.'dashboard/error/1');
		}
    } 	
}

/* End of file */?>