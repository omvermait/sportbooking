<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Common extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
	}
	
	/* State List by Country ID */
	public function getStateListByCountryID()
	{
		$country_id = $this->input->post('country_id');
		$state_list = $this->common_model->getData('com_state', array('country_id'=>$country_id), 'multi');
		$html = '';
		$html .= '<option value="">-- Select --</option>';
		if(!empty($state_list))
		{
			foreach ($state_list as $s_list) 
			{
				$html .= '<option value="'.$s_list->state_id.'">'.$s_list->state_name.'</option>';
			}
			echo $html; 
		}
		else
		{
			echo $html;
		}
	}

	/* City List by State ID */
	public function getCityListByStateID()
	{
		$state_id = $this->input->post('state_id');
		$city_list = $this->common_model->getData('com_city', array('state_id'=>$state_id), 'multi');
		$html = '';
		$html .= '<option value="">-- Select --</option>';
		if(!empty($city_list))
		{
			foreach ($city_list as $c_list) 
			{
				$html .= '<option value="'.$c_list->city_id.'">'.$c_list->city_name.'</option>';
			}
			echo $html; 
		}
		else
		{
			echo $html;
		}
	}

}

/* End of file */?>