<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Game extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		if(!empty(MODULE_NAME))
		{
		   $this->load->model(MODULE_NAME.'game_model');
		}   
	}
	
	/*	Validation Rules */
	protected $validation_rules = array
        (
        'gameAdd' => array(
            array(
                'field' => 'game_name',
                'label' => 'Game Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'game_status',
                'label' => 'Game status',
                'rules' => 'trim|required'
            )  
        ),
		'gameUpdate' => array(
            array(
                'field' => 'game_name',
                'label' => 'Game Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'game_status',
                'label' => 'Game status',
                'rules' => 'trim|required'
            )
        )
    );

	/* Details */
	public function index()
	{
		if($this->checkViewPermission())
		{			
			$this->show_view(MODULE_NAME.'game/game_view', $this->data);
		}
		else
		{	
			redirect( base_url().MODULE_NAME.'dashboard/error/1');
		}
    } 

    public function loadData()
    {
    	$result = $this->game_model->getAllDataList();
    	$data = array();
        $no = $_POST['start'];
        foreach ($result as $e_res) 
	    {
			$no++;
			$row   = array();
			$row[] = $no;
			$row[] = (!empty($e_res->game_img)) ? '<img width="50px" src="'.base_url().''.$e_res->game_img.'">' : '<img width="50px" src="'.base_url().'webroot/upload/dummy/dummy.png">';
			$row[] = $e_res->game_name;
			$play_members_str = '';
			$play_members_id_arr = explode(',', $e_res->play_members_id);
			if(!empty($play_members_id_arr)){
				foreach($play_members_id_arr as $play_members_id) {
					$play_members_res = $this->common_model->getData('tbl_play_members', array('play_members_id'=>$play_members_id), 'single');
					if(!empty($play_members_res)){
						$play_members_str .= $play_members_res->play_members.'<br>';
					}
				}
			}
			$row[] = $play_members_str;
			$row[] = viewStatus ($e_res->game_status);
	 		$btn = '';
	 		if($this->checkViewPermission())
	 		{
	 			$btn .= '<a class="btn btn-success btn-sm" href="'.base_url().''.MODULE_NAME.'game/gameView/'.$e_res->game_id.'" title="View"><i class="fa fa-eye fa-1x "></i></a>&nbsp;&nbsp;';
	 		}
	 		if($this->checkEditPermission())
	 		{
	 			$btn .= '<a class="btn btn-success btn-sm" href="'.base_url().''.MODULE_NAME.'game/addGame/'.$e_res->game_id.'" title="Edit"><i class="fa fa-edit fa-1x "></i></a>&nbsp;&nbsp;';
	 		}
	 		if($this->checkDeletePermission())
	 		{
	 			$btn .= '<a class="confirm btn btn-danger btn-sm" onclick="return confirm(\'Are you sure you want to Delete\')" href="'.base_url().''.MODULE_NAME.'game/deleteGame/'.$e_res->game_id.'" title="Remove"><i class="fa fa-trash-o fa-1x" data-toggle="modal" data-target=".bs-example-modal-sm"></i></a>';
	 		}
	 		$row[] = $btn;
            $data[] = $row;
        }

        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => count($result),
			"recordsFiltered" => $this->game_model->count_filtered(),
			"data" => $data,
		);
       	//output to json format
       	echo json_encode($output);
    }
    /* full  Details */
    public function gameView()
	{
		if($this->checkViewPermission())
		{			
			$game_id = $this->uri->segment(4);
			$this->data['edit_game'] = $this->common_model->getData('tbl_game', array('game_id'=>$game_id), 'single');
			if(!empty($this->data['edit_game']))
			{
				$this->show_view(MODULE_NAME.'game/game_full_view', $this->data);
			}	
			else
			{
				redirect(base_url().MODULE_NAME.'game');
			}
		}
		else
		{	
			redirect( base_url().MODULE_NAME.'dashboard/error/1');
		}
    }

    /* Add & update */
    public function addGame()
    {
    	$game_id = $this->uri->segment(4);
		if($game_id)
		{
			if($this->checkEditPermission())
			{
				if (isset($_POST['Submit']) && $_POST['Submit'] == "Edit") 
				{                   
					$this->form_validation->set_rules($this->validation_rules['gameUpdate']);
					if($this->form_validation->run())
					{
                    	$post['game_name'] = $this->input->post('game_name');	
						$post['game_status'] = $this->input->post('game_status');
						$post['play_members_id'] = implode(',', $this->input->post('play_members_id'));

						if($_FILES["game_img"]["name"]) 
						{
	                       $game_img = 'game_img';
	                       $fieldName = "game_img";
	                       $Path = 'webroot/upload/game';
	                       $game_img = $this->ImageUpload($_FILES["game_img"]["name"], $game_img, $Path, $fieldName);
	                       $post['game_img'] = $Path.'/'.$game_img;
	                   	}

						$post['game_created_date'] = date('Y-m-d');
						$post['game_updated_date'] = date('Y-m-d');
                        $n_post = $this->xssCleanValidate($post);
	                   	$this->common_model->updateData('tbl_game', array('game_id'=>$game_id), $n_post); 
	                   	$msg = 'Game updated successfully!!';					
						$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
						redirect(base_url().MODULE_NAME.'game');
					}
					else
					{
						$this->data['edit_game'] = $this->common_model->getData('tbl_game', array('game_id'=>$game_id), 'single');
						if(!empty($this->data['edit_game']))
						{
							$this->show_view(MODULE_NAME.'game/game_update', $this->data);
						}	
						else
						{
							redirect(base_url().MODULE_NAME.'game');
						}
					}
				}
				else
				{
					$this->data['edit_game'] = $this->common_model->getData('tbl_game', array('game_id'=>$game_id), 'single');
					if(!empty($this->data['edit_game']))
					{
						$this->show_view(MODULE_NAME.'game/game_update', $this->data);
					}	
					else
					{
						redirect(base_url().MODULE_NAME.'game');
					}
				}
			}
			else
			{	
				redirect( base_url().MODULE_NAME.'dashboard/error/1');
			}
		}
		else
		{
			if($this->checkAddPermission())
			{
				if (isset($_POST['Submit']) && $_POST['Submit'] == "Add") 
				{
					$this->form_validation->set_rules($this->validation_rules['gameAdd']);
					if($this->form_validation->run())
					{
                    	$post['game_name'] = $this->input->post('game_name');	
						$post['game_status'] = $this->input->post('game_status');
						$post['play_members_id'] = implode(',', $this->input->post('play_members_id'));

						if($_FILES["game_img"]["name"]) 
						{
	                       $game_img = 'game_img';
	                       $fieldName = "game_img";
	                       $Path = 'webroot/upload/game';
	                       $game_img = $this->ImageUpload($_FILES["game_img"]["name"], $game_img, $Path, $fieldName);
	                       $game_img = $Path.'/'.$game_img;
	                   	}
	                   	else{
	                       $game_img = base_url().'upload/dummy/dummy.png';
	                   	}
	                    $post['game_img'] = $game_img;

						$post['game_created_date'] = date('Y-m-d');
						$post['game_updated_date'] = date('Y-m-d');
                        $n_post = $this->xssCleanValidate($post);
						$this->common_model->addData('tbl_game', $n_post);
	                   	$msg = 'Game added successfully!!';					
						$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
						redirect(base_url().MODULE_NAME.'game');
	                }
					else
					{
						$this->show_view(MODULE_NAME.'game/game_add', $this->data);
					}
				}
				else
				{
					$this->show_view(MODULE_NAME.'game/game_add', $this->data);
				}
			}
			else
			{	
				redirect( base_url().MODULE_NAME.'dashboard/error/1');
			}
		}
    }

    /* Delete */
	public function deleteGame()
	{
		if($this->checkDeletePermission())
		{
			$game_id = $this->uri->segment(4);
			$n_post['game_status'] = '2';
			$this->common_model->updateData('tbl_game', array('game_id'=>$game_id), $n_post); 
			$msg = 'Game remove successfully...!';					
			$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
			redirect(base_url().MODULE_NAME.'game');
		}
	}
	
}

/* End of file */?>