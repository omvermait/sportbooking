<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CenterDetail extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		if(!empty(MODULE_NAME))
		{
			$this->load->model(MODULE_NAME.'centeradetail_model');
		}
	}

	/*	Validation Rules */
	 protected $validation_rules = array
        (
        'centerDetailAdd' => array(
            array(
                'field' => 'center_name',
                'label' => 'Center Name',
                'rules' => 'trim|required'
            ) 
        ),
		'centerDetailUpdate' => array(
            array(
                'field' => 'center_name',
                'label' => 'Center Name',
                'rules' => 'trim|required'
            )
        )
    );
	
	public function index()
	{
		if($this->checkViewPermission())
		{			
			$this->show_view(MODULE_NAME.'centerDetail/centerDetail_view', $this->data);
		}
		else
		{	
			redirect( base_url().MODULE_NAME.'dashboard/error/1');
		}
    }

    public function loadCenterListData()
    {
    	$center_list = $this->centeradetail_model->getAllCenterList();
    	$data = array();
        $no = $_POST['start'];
        foreach ($center_list as $c_res) 
	    {
			$no++;
			$row   = array();
			$row[] = $no;
			if(!empty($c_res->center_logo))
			{
				$row[] = '<img width="50px" src="'.base_url().''.$c_res->center_logo.'">';
			}
			else
			{
				$row[] = '<img width="50px" src="'.base_url().'webroot/upload/dummy/dummy.png">';
			}

			$row[] = $c_res->center_name;
			$row[] = $c_res->center_email;
			$row[] = $c_res->center_contact_number;
			$row[] = $c_res->center_address;
			$services_str = ''; 
			$services_arr = explode(',', $c_res->services_id);
			if(!empty($services_arr)){
				foreach ($services_arr as $services_id) {
					$services_res = $this->common_model->getData('tbl_services', array('services_id'=>$services_id), 'single');
					if(!empty($services_res)){
						$services_str .= $services_res->services_name.'<br>';
					}
				}
			}
			$row[] = $services_str;
			$row[] = viewStatus ($c_res->center_status);
	 		$btn = '';
	 		if($this->checkViewPermission())
	 		{
	 			$btn .= '<a class="btn btn-success btn-sm" href="'.base_url().''.MODULE_NAME.'centerDetail/centerDetailView/'.$c_res->center_id.'" title="View"><i class="fa fa-eye fa-1x "></i></a>&nbsp;&nbsp;';
	 		}
	 		if($this->checkEditPermission())
	 		{
	 			$btn .= '<a class="btn btn-success btn-sm" href="'.base_url().''.MODULE_NAME.'centerDetail/addCenterDetail/'.$c_res->center_id.'" title="Edit"><i class="fa fa-edit fa-1x "></i></a>&nbsp;&nbsp;';
	 		}
	 		if($this->checkDeletePermission())
	 		{
	 			$btn .= '<a class="confirm btn btn-danger btn-sm" onclick="return confirm(\'Are you sure you want to Delete\')" href="'.base_url().''.MODULE_NAME.'centerDetail/deleteCenterDetail/'.$c_res->center_id.'" title="Remove"><i class="fa fa-trash-o fa-1x" data-toggle="modal" data-target=".bs-example-modal-sm"></i></a>';
	 		}
	 		$row[] = $btn;
            $data[] = $row;
        }

        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => count($center_list),
			"recordsFiltered" => $this->centeradetail_model->count_filtered(),
			"data" => $data,
		);
       	//output to json format
       	echo json_encode($output);
    }

    public function centerDetailView()
	{
		if($this->checkViewPermission())
		{			
			$center_id = $this->uri->segment(4);
			$this->data['edit_center_detail'] = $this->common_model->getData('tbl_center', array('center_id'=>$center_id), 'single');
			if(!empty($this->data['edit_center_detail']))
			{
				$this->show_view(MODULE_NAME.'centerDetail/centerDetail_full_view', $this->data);
			}
			else
			{
				redirect(base_url().MODULE_NAME.'centerDetail');
			}
		}
		else
		{	
			redirect( base_url().MODULE_NAME.'dashboard/error/1');
		}
    }

    /* Add & update */
    public function addCenterDetail()
    {
    	$center_id = $this->uri->segment(4);
		if($center_id)
		{
			if($this->checkEditPermission())
			{
				if (isset($_POST['Submit']) && $_POST['Submit'] == "Edit") 
				{
					$this->form_validation->set_rules($this->validation_rules['centerDetailUpdate']);
					if($this->form_validation->run())
					{
						$post['center_name'] = $this->input->post('center_name');
						$post['center_email'] = $this->input->post('center_email');
						$post['center_contact_number'] = $this->input->post('center_contact_number');
						$post['center_address'] = $this->input->post('center_address');
						$post['center_status'] = $this->input->post('center_status');
						$post['services_id'] = implode(',', $this->input->post('services_id'));
						$post['center_description'] = $this->input->post('center_description');
						$post['center_updated_date'] = date('Y-m-d');

						if($_FILES["center_logo"]["name"]) 
						{
	                       	$center_logo = 'center_logo';
	                       	$fieldName = "center_logo";
	                       	$Path = 'webroot/upload/center/profile';
	                       	$center_logo = $this->ImageUpload($_FILES["center_logo"]["name"], $center_logo, $Path, $fieldName);
	                       	if(!empty($center_logo)){
	                       		$post['center_logo'] = $Path.'/'.$center_logo;
	                       	}
	                   	}
						$n_post = $this->xssCleanValidate($post);
	                   	$this->common_model->updateData('tbl_center', array('center_id'=>$center_id), $n_post); 

	                   	if($_FILES["center_img"]['name'])
						{
							$center_img = $_FILES["center_img"]['name'];
							for ($i=0; $i<count($center_img); $i++) 
							{ 
								if(!empty($_FILES['center_img']['name'][$i])){
									$_FILES['new_img_file']['name'] = $_FILES['center_img']['name'][$i];
			        				$_FILES['new_img_file']['type'] = $_FILES['center_img']['type'][$i];
					                $_FILES['new_img_file']['tmp_name'] = $_FILES['center_img']['tmp_name'][$i];
					                $_FILES['new_img_file']['error'] = $_FILES['center_img']['error'][$i];
					                $_FILES['new_img_file']['size'] = $_FILES['center_img']['size'][$i];
					                $img_path = 'webroot/upload/center/';
					                $img_fieldName = 'new_img_file';
					                $img_name = 'center_img';
					                $allowed_types = 'GIF | gif | JPE | jpe | JPEG | jpeg | JPG | jpg | PNG | png';
					                $imageName = $this->FileUpload($_FILES["new_img_file"]["name"], $img_name, $img_path, $img_fieldName, $allowed_types);
					                $img_name = (!empty($imageName)) ? $img_path.''.$imageName : 'upload/dummy/dummy.png';
					                $post_img['center_img'] = $img_name;
		                            $post_img['center_id'] =  $center_id;
		                            $post_img['center_img_status'] = '1';
		                            $this->common_model->addData('tbl_center_img', $post_img);
		                        }
							}
						} 
	 
	                   	$msg = 'Center Detail Updated Successfully!!';					
						$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
						redirect(base_url().MODULE_NAME.'centerDetail');
					}
					else
					{
						$this->data['edit_center_detail'] = $this->common_model->getData('tbl_center', array('center_id'=>$center_id), 'single');	
						if(!empty($this->data['edit_center_detail']))
						{
							$this->show_view(MODULE_NAME.'centerDetail/centerDetail_update', $this->data);
						}
						else
						{
							redirect(base_url().MODULE_NAME.'centerDetail');
						}
					}
				}
				else
				{
					$this->data['edit_center_detail'] = $this->common_model->getData('tbl_center', array('center_id'=>$center_id), 'single');	
					if(!empty($this->data['edit_center_detail']))
					{
						$this->show_view(MODULE_NAME.'centerDetail/centerDetail_update', $this->data);
					}
					else
					{
						redirect(base_url().MODULE_NAME.'centerDetail');
					}
				}
			}
			else
			{	
				redirect( base_url().MODULE_NAME.'dashboard/error/1');
			}
		}
		else
		{
			if($this->checkAddPermission())
			{
				if (isset($_POST['Submit']) && $_POST['Submit'] == "Add") 
				{
					$this->form_validation->set_rules($this->validation_rules['centerDetailAdd']);
					if($this->form_validation->run())
					{
						$post['center_name'] = $this->input->post('center_name');
						$post['center_email'] = $this->input->post('center_email');
						$post['center_contact_number'] = $this->input->post('center_contact_number');
						$post['center_address'] = $this->input->post('center_address');
						$post['center_status'] = $this->input->post('center_status');
						$post['services_id'] = implode(',', $this->input->post('services_id'));
						$post['center_description'] = $this->input->post('center_description');
						$post['center_created_date'] = date('Y-m-d');
						$post['center_updated_date'] = date('Y-m-d');

						if($_FILES["center_logo"]["name"]) 
						{
	                       	$center_logo = 'center_logo';
	                       	$fieldName = "center_logo";
	                       	$Path = 'webroot/upload/center/profile';
	                       	$center_logo = $this->ImageUpload($_FILES["center_logo"]["name"], $center_logo, $Path, $fieldName);
	                       	if(!empty($center_logo)){
	                       		$post['center_logo'] = $Path.'/'.$center_logo;
	                       	}
	                       	else{
	                       		$post['center_logo'] = 'webroot/upload/dummy/dummy.png';
	                       	}
	                   	}

						$n_post = $this->xssCleanValidate($post);
						$center_id = $this->common_model->addData('tbl_center', $n_post);


						if($_FILES["center_img"]['name'])
						{
							$center_img = $_FILES["center_img"]['name'];
							for ($i=0; $i<count($center_img); $i++) 
							{ 
								$_FILES['new_img_file']['name'] = $_FILES['center_img']['name'][$i];
		        				$_FILES['new_img_file']['type'] = $_FILES['center_img']['type'][$i];
				                $_FILES['new_img_file']['tmp_name'] = $_FILES['center_img']['tmp_name'][$i];
				                $_FILES['new_img_file']['error'] = $_FILES['center_img']['error'][$i];
				                $_FILES['new_img_file']['size'] = $_FILES['center_img']['size'][$i];
				                $img_path = 'webroot/upload/center/';
				                $img_fieldName = 'new_img_file';
				                $img_name = 'center_img';
				                $allowed_types = 'GIF | gif | JPE | jpe | JPEG | jpeg | JPG | jpg | PNG | png';
				                $imageName = $this->FileUpload($_FILES["new_img_file"]["name"], $img_name, $img_path, $img_fieldName, $allowed_types);
				                $img_name = (!empty($imageName)) ? $img_path.''.$imageName : 'upload/dummy/dummy.png';
				                $post_img['center_img'] = $img_name;
	                            $post_img['center_id'] =  $center_id;
	                            $post_img['center_img_status'] = '1';
	                            $this->common_model->addData('tbl_center_img', $post_img);
							}
						} 
				
	                   	$msg = 'Center Detail Added Successfully!!';					
						$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
						redirect(base_url().MODULE_NAME.'centerDetail');
	                }
					else
					{
						$this->show_view(MODULE_NAME.'centerDetail/centerDetail_add', $this->data);
					}
				}
				else
				{
					    $this->show_view(MODULE_NAME.'centerDetail/centerDetail_add', $this->data);
				}
			}
			else
			{	
				redirect( base_url().MODULE_NAME.'dashboard/error/1');
			}
		}
    }

    /* Delete */
	public function deleteCenterDetail()
	{
		if($this->checkDeletePermission())
		{
			$center_id = $this->uri->segment(4);	
			$n_post['center_status'] = '2';
			$this->common_model->updateData('tbl_center', array('center_id'=>$center_id), $n_post);
			$msg = 'Center Detail Remove Successfully...!';					
			$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
			redirect(base_url().MODULE_NAME.'centerDetail');
		}
	}

	public function removeCenterImage()
	{
		$center_img_id = $this->input->post('center_img_id');
		$this->common_model->deleteData('tbl_center_img', array('center_img_id'=>$center_img_id)); 
		echo "1";
	}
}
/* End of file */?>