<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Quality extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		if(!empty(MODULE_NAME))
		{
		   $this->load->model(MODULE_NAME.'quality_model');
		}   
	}
	
	/*	Validation Rules */
	protected $validation_rules = array
        (
        'qualityAdd' => array(
            array(
                'field' => 'quality_name',
                'label' => 'Quality Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'quality_status',
                'label' => 'Quality status',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'center_id',
                'label' => 'Center',
                'rules' => 'trim|required'
            )  
        ),
		'qualityUpdate' => array(
            array(
                'field' => 'quality_name',
                'label' => 'Quality Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'quality_status',
                'label' => 'Quality status',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'center_id',
                'label' => 'Center',
                'rules' => 'trim|required'
            )
        )
    );

	/* Details */
	public function index()
	{
		if($this->checkViewPermission())
		{			
			$this->show_view(MODULE_NAME.'quality/quality_view', $this->data);
		}
		else
		{	
			redirect( base_url().MODULE_NAME.'dashboard/error/1');
		}
    } 

    public function loadData()
    {
    	$result = $this->quality_model->getAllDataList();
    	$data = array();
        $no = $_POST['start'];
        foreach ($result as $e_res) 
	    {
			$no++;
			$row   = array();
			$row[] = $no;
			$center_id = substr($e_res->center_id, 2, 10);
			$center_res = $this->common_model->getTableValue('tbl_center', 'center_id', $center_id);
			$row[] = !empty($center_res) ? $center_res->center_name : '';
			$row[] = (!empty($e_res->quality_img)) ? '<img width="50px" src="'.base_url().''.$e_res->quality_img.'">' : '<img width="50px" src="'.base_url().'webroot/upload/dummy/dummy.png">';
			$row[] = $e_res->quality_name;
			$row[] = viewStatus ($e_res->quality_status);
	 		$btn = '';
	 		if($this->checkViewPermission())
	 		{
	 			$btn .= '<a class="btn btn-success btn-sm" href="'.base_url().''.MODULE_NAME.'quality/qualityView/'.$e_res->quality_id.'" title="View"><i class="fa fa-eye fa-1x "></i></a>&nbsp;&nbsp;';
	 		}
	 		if($this->checkEditPermission())
	 		{
	 			$btn .= '<a class="btn btn-success btn-sm" href="'.base_url().''.MODULE_NAME.'quality/addQuality/'.$e_res->quality_id.'" title="Edit"><i class="fa fa-edit fa-1x "></i></a>&nbsp;&nbsp;';
	 		}
	 		if($this->checkDeletePermission())
	 		{
	 			$btn .= '<a class="confirm btn btn-danger btn-sm" onclick="return confirm(\'Are you sure you want to Delete\')" href="'.base_url().''.MODULE_NAME.'quality/deleteQuality/'.$e_res->quality_id.'" title="Remove"><i class="fa fa-trash-o fa-1x" data-toggle="modal" data-target=".bs-example-modal-sm"></i></a>';
	 		}
	 		$row[] = $btn;
            $data[] = $row;
        }

        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => count($result),
			"recordsFiltered" => $this->quality_model->count_filtered(),
			"data" => $data,
		);
       	//output to json format
       	echo json_encode($output);
    }
    /* full  Details */
    public function qualityView()
	{
		if($this->checkViewPermission())
		{			
			$quality_id = $this->uri->segment(4);
			$this->data['edit_quality'] = $this->common_model->getData('tbl_quality', array('quality_id'=>$quality_id), 'single');
			if(!empty($this->data['edit_quality']))
			{
				$this->show_view(MODULE_NAME.'quality/quality_full_view', $this->data);
			}	
			else
			{
				redirect(base_url().MODULE_NAME.'quality');
			}
		}
		else
		{	
			redirect( base_url().MODULE_NAME.'dashboard/error/1');
		}
    }

    /* Add & update */
    public function addQuality()
    {
    	$quality_id = $this->uri->segment(4);
		if($quality_id)
		{
			if($this->checkEditPermission())
			{
				if (isset($_POST['Submit']) && $_POST['Submit'] == "Edit") 
				{                   
					$this->form_validation->set_rules($this->validation_rules['qualityUpdate']);
					if($this->form_validation->run())
					{
						$post['center_id'] = '0,'.$this->input->post('center_id');
                    	$post['quality_name'] = $this->input->post('quality_name');	
						$post['quality_status'] = $this->input->post('quality_status');

						if($_FILES["quality_img"]["name"]) 
						{
	                       $quality_img = 'quality_img';
	                       $fieldName = "quality_img";
	                       $Path = 'webroot/upload/quality';
	                       $quality_img = $this->ImageUpload($_FILES["quality_img"]["name"], $quality_img, $Path, $fieldName);
	                       $post['quality_img'] = $Path.'/'.$quality_img;
	                   	}

						$post['quality_created_date'] = date('Y-m-d');
						$post['quality_updated_date'] = date('Y-m-d');
                        $n_post = $this->xssCleanValidate($post);
	                   	$this->common_model->updateData('tbl_quality', array('quality_id'=>$quality_id), $n_post); 
	                   	$msg = 'Quality updated successfully!!';					
						$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
						redirect(base_url().MODULE_NAME.'quality');
					}
					else
					{
						$this->data['edit_quality'] = $this->common_model->getData('tbl_quality', array('quality_id'=>$quality_id), 'single');
						if(!empty($this->data['edit_quality']))
						{
							$this->show_view(MODULE_NAME.'quality/quality_update', $this->data);
						}	
						else
						{
							redirect(base_url().MODULE_NAME.'quality');
						}
					}
				}
				else
				{
					$this->data['edit_quality'] = $this->common_model->getData('tbl_quality', array('quality_id'=>$quality_id), 'single');
					if(!empty($this->data['edit_quality']))
					{
						$this->show_view(MODULE_NAME.'quality/quality_update', $this->data);
					}	
					else
					{
						redirect(base_url().MODULE_NAME.'quality');
					}
				}
			}
			else
			{	
				redirect( base_url().MODULE_NAME.'dashboard/error/1');
			}
		}
		else
		{
			if($this->checkAddPermission())
			{
				if (isset($_POST['Submit']) && $_POST['Submit'] == "Add") 
				{
					$this->form_validation->set_rules($this->validation_rules['qualityAdd']);
					if($this->form_validation->run())
					{
						$post['center_id'] = '0,'.$this->input->post('center_id');
                    	$post['quality_name'] = $this->input->post('quality_name');	
						$post['quality_status'] = $this->input->post('quality_status');

						if($_FILES["quality_img"]["name"]) 
						{
	                       $quality_img = 'quality_img';
	                       $fieldName = "quality_img";
	                       $Path = 'webroot/upload/quality';
	                       $quality_img = $this->ImageUpload($_FILES["quality_img"]["name"], $quality_img, $Path, $fieldName);
	                       $quality_img = $Path.'/'.$quality_img;
	                   	}
	                   	else{
	                       $quality_img = base_url().'upload/dummy/dummy.png';
	                   	}
	                    $post['quality_img'] = $quality_img;

						$post['quality_created_date'] = date('Y-m-d');
						$post['quality_updated_date'] = date('Y-m-d');
                        $n_post = $this->xssCleanValidate($post);
						$this->common_model->addData('tbl_quality', $n_post);
	                   	$msg = 'Quality added successfully!!';					
						$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
						redirect(base_url().MODULE_NAME.'quality');
	                }
					else
					{
						$this->show_view(MODULE_NAME.'quality/quality_add', $this->data);
					}
				}
				else
				{
					$this->show_view(MODULE_NAME.'quality/quality_add', $this->data);
				}
			}
			else
			{	
				redirect( base_url().MODULE_NAME.'dashboard/error/1');
			}
		}
    }

    /* Delete */
	public function deleteQuality()
	{
		if($this->checkDeletePermission())
		{
			$quality_id = $this->uri->segment(4);
			$n_post['quality_status'] = '2';
			$this->common_model->updateData('tbl_quality', array('quality_id'=>$quality_id), $n_post); 
			$msg = 'Quality remove successfully...!';					
			$this->session->set_flashdata('message', '<section><div class="col-xs-12"><div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div></div></section>');
			redirect(base_url().MODULE_NAME.'quality');
		}
	}
	
}

/* End of file */?>