<aside class="right-side">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>Quality</h1>
      <ol class="breadcrumb">
         <li><a href="<?php echo base_url().MODULE_NAME;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="<?php echo base_url().MODULE_NAME;?>quality">Quality</a></li>
         <li class="active">View Quality </li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="box box-success">
         <div class="box-header">
            <div class="pull-left">
               <h3 class="box-title">View Quality </h3>
            </div>
            <div class="pull-right box-tools">
               <a href="<?php echo base_url().MODULE_NAME;?>quality" class="btn btn-info btn-sm">Back</a>
            </div>
         </div>
         <form action="" method="post" accept-charset="utf-8" enctype="multipart/form-data">
            <?php  $csrf = array( 'name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash() ); ?>
            <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
            <div class="box-body">
               <br>
               <div>
                  <div id="msg_div">
                     <?php echo $this->session->flashdata('message');?>
                  </div>
               </div>
                <div class="row">
                    <div class="form-group col-md-4">
                        <div class="input text">
                            <label>Center<span class="text-danger">*</span></label>
                            <select disabled data-validation="required" name="center_id" id="center_id" class="selectpicker form-control" data-show-subtext="true" data-live-search="true">
                                <?php
                                $center_list= $this->common_model->getData('tbl_center', array('center_status'=>'1'), 'multi');
                                    foreach($center_list as $val) 
                                    {
                                        ?>
                                           <option <?php if($edit_quality->center_id == $val->center_id){ echo "selected"; } ?> value="<?php echo $val->center_id; ?>"><?php echo $val->center_name; ?></option>
                                        <?php 
                                    }
                                ?>
                            </select>
                            <?php echo form_error('center_id','<span class="text-danger">','</span>'); ?>
                        </div>
                    </div>
                </div>
               <div class="row">
                  <div class="form-group col-md-4">
                     <div class="input text">
                        <label>Quality Name<span class="text-danger">*</span></label>
                        <input disabled required data-validation="alphanumeric" data-validation-allowing="- _" name="quality_name" id="quality_name" class="form-control" type="text" value="<?php echo $edit_quality->quality_name; ?>" />
                        <?php echo form_error('quality_name','<span class="text-danger">','</span>'); ?>
                     </div>
                  </div>
                  <div class="form-group col-md-4">
                     <div class="input text">
                        <label>Quality Status<span class="text-danger">*</span></label>
                        <select disabled data-validation="required" name="quality_status" id="quality_status" class="form-control">
                           <option <?php echo ($edit_quality->quality_status == 1) ? 'selected' : ''; ?> value="1">Active</option>
                           <option <?php echo ($edit_quality->quality_status == 0) ? 'selected' : ''; ?> value="0">Inactive</option>
                        </select>
                        <?php echo form_error('quality_status','<span class="text-danger">','</span>'); ?>
                     </div>
                  </div>
                    <div class="form-group col-md-2">
                        <div class="input text">
                            <label>Quality Image</label>
                            <?php
                                if(!empty($edit_quality->quality_img))
                                {
                                    ?>
                                    <img width="100px" src="<?php echo base_url().''.$edit_quality->quality_img; ?>">
                                    <?php
                                }
                                else
                                {
                                    ?>
                                    <img width="100px" src="<?php echo base_url().'webroot/upload/dummy/user.png'; ?>">
                                    <?php
                                }
                            ?>
                        </div>
                    </div> 
               </div>
               <!-- /.box-body -->      
               <div class="box-footer">
                  <a class="btn btn-danger btn-sm" href="<?php echo base_url().MODULE_NAME;?>quality">Cancel</a>
               </div>
         </form>
      </div>
      <!-- /.box -->
   </section>
   <!-- /.content -->
</aside>
<!-- /.right-side -->