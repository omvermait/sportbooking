<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Customer</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url().MODULE_NAME;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo base_url().MODULE_NAME;?>customer">Customer</a></li>
            <li class="active">Update Customer</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">       
        <div class="box box-success">
            <div class="box-header">
                <div class="pull-left">
                    <h3 class="box-title">Update Customer</h3>
                </div>
                <div class="pull-right box-tools">
                    <a href="<?php echo base_url().MODULE_NAME;?>customer" class="btn btn-info btn-sm">Back</a>
                </div>
            </div>
            <form action="" method="post" accept-charset="utf-8" enctype="multipart/form-data" id="login_form">
                <?php  $csrf = array( 'name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash() ); ?>
                <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                <div class="box-body">
                    <div>
                        <div id="msg_div">
                            <?php echo $this->session->flashdata('message');?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Name<span class="text-danger">*</span></label>
                                <input required data-validation="custom" data-validation-regexp="^([a-zA-z -]+)$" data-validation-allowing="- _" name="customer_name" id="customer_name" class="form-control" type="text" value="<?php echo $edit_customer->customer_name; ?>" />
                                <?php echo form_error('customer_name','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Username<span class="text-danger">*</span></label>
                                <input readonly required="required" name="login_name" id="login_name" class="form-control" type="text" value="<?php echo $edit_customer->login_name; ?>" />
                                <?php echo form_error('login_name','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Email<span class="text-danger">*</span></label>
                                <input readonly required="required" data-validation="email" name="customer_email" id="customer_email" class="form-control" type="email" value="<?php echo $edit_customer->customer_email; ?>" />
                                <?php echo form_error('customer_email','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div> 
                    </div> 
                    <div class="row">
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Password<span class="text-danger">*</span></label>
                                <input name="customer_password" id="customer_password" class="form-control" type="password" value="" />
                                <?php echo form_error('customer_password','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Confirm Password<span class="text-danger">*</span></label>
                                <input data-validation="confirmation" data-validation-confirm="customer_password" name="customer_cpassword" id="customer_cpassword" class="form-control" type="password" value="" />
                                <?php echo form_error('customer_cpassword','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Address<span class="text-danger">*</span></label>
                                <textarea data-validation="required" name="customer_address" id="customer_address" class="form-control"><?php echo $edit_customer->customer_address; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Status<span class="text-danger">*</span></label>
                                <select data-validation="required" name="customer_status" id="customer_status" class="form-control">
                                    <option <?php if($edit_customer->customer_status == '1'){ echo "selected"; } ?> value="1">Active</option>
                                    <option <?php if($edit_customer->customer_status == '0'){ echo "selected"; } ?> value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class="input text">
                                <label>Profile Image</label>
                                <?php
                                    if(!empty($edit_customer->customer_profile_img))
                                    {
                                        ?>
                                        <img width="100px" src="<?php echo base_url().''.$edit_customer->customer_profile_img; ?>">
                                        <?php
                                    }
                                    else
                                    {
                                        ?>
                                        <img width="100px" src="<?php echo base_url().'webroot/upload/admin/customers/customer.png'; ?>">
                                        <?php
                                    }
                                ?>
                            </div>
                        </div> 
                        <div class="form-group col-md-2">
                            <div class="input text">
                                <label>&nbsp;</label>
                                <input data-validation="mime size" data-validation-allowing="jpg, png, gif, jpeg, jpe" data-validation-max-size="3M" name="customer_profile_img" type="file" id="customer_profile_img" value="" />
                                <small>Max upload size is 3MB</small>
                            </div>
                            <span class="text-danger" id="error_id"></span>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->      
                <div class="box-footer">
                    <button class="btn btn-success btn-sm" type="submit" name="Submit" value="Edit" >Submit</button>
                    <a class="btn btn-danger btn-sm" href="<?php echo base_url().MODULE_NAME;?>customer">Cancel</a>
                </div>
            </form>                    
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</aside>
<!-- /.right-side -->