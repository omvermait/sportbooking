<aside class="right-side">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>Dashboard</h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Dashboard</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="box box-success">
         <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <h3 class="box-title"><?php echo (!empty($center_name)) ? 'Welcome, '.$center_name : '';?></h3>
              </div>
            </div>
         </div>
      </div>    
   </section>
   <!-- /.content -->
</aside>