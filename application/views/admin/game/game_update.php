<aside class="right-side">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>Game</h1>
      <ol class="breadcrumb">
         <li><a href="<?php echo base_url().MODULE_NAME;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="<?php echo base_url().MODULE_NAME;?>game">Game</a></li>
         <li class="active">Update Game </li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="box box-success">
         <div class="box-header">
            <div class="pull-left">
               <h3 class="box-title">Update Game </h3>
            </div>
            <div class="pull-right box-tools">
               <a href="<?php echo base_url().MODULE_NAME;?>game" class="btn btn-info btn-sm">Back</a>
            </div>
         </div>
         <form action="" method="post" accept-charset="utf-8" enctype="multipart/form-data">
            <?php  $csrf = array( 'name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash() ); ?>
            <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
            <div class="box-body">
               <br>
               <div>
                  <div id="msg_div">
                     <?php echo $this->session->flashdata('message');?>
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-md-4">
                     <div class="input text">
                        <label>Game Name<span class="text-danger">*</span></label>
                        <input required data-validation="alphanumeric" data-validation-allowing="- _" name="game_name" id="game_name" class="form-control" type="text" value="<?php echo $edit_game->game_name; ?>" />
                        <?php echo form_error('game_name','<span class="text-danger">','</span>'); ?>
                     </div>
                  </div>
                  <div class="form-group col-md-4">
                     <div class="input text">
                        <label>Game Status<span class="text-danger">*</span></label>
                        <select data-validation="required" name="game_status" id="game_status" class="form-control">
                           <option <?php echo ($edit_game->game_status == 1) ? 'selected' : ''; ?> value="1">Active</option>
                           <option <?php echo ($edit_game->game_status == 0) ? 'selected' : ''; ?> value="0">Inactive</option>
                        </select>
                        <?php echo form_error('game_status','<span class="text-danger">','</span>'); ?>
                     </div>
                  </div>
                  <div class="form-group col-md-4">
                     <div class="input text">
                        <label>Team<span class="text-danger">*</span></label>
                        <select data-validation="required" name="play_members_id[]" id="play_members_id" class="form-control selectpicker" data-live-search="true" multiple>
                           <?php
                              $play_members_id_arr = explode(',', $edit_game->play_members_id);
                              $play_members_res = $this->common_model->getData('tbl_play_members', array('play_members_status'=>'1'), 'multi');
                              if(!empty($play_members_res)){
                                 foreach($play_members_res as $pm_val) {
                                    ?>
                                    <option <?php if(!empty($play_members_id_arr)){ echo (in_array($pm_val->play_members_id, $play_members_id_arr)) ? 'selected' : ''; } ?> value="<?php echo $pm_val->play_members_id; ?>"><?php echo $pm_val->play_members; ?></option>
                                    <?php
                                 }
                              }
                           ?>
                        </select>
                        <?php echo form_error('play_members_id','<span class="text-danger">','</span>'); ?>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-md-2">
                     <div class="input text">
                        <label>Game Image</label>
                        <?php
                           if(!empty($edit_game->game_img))
                           {
                              ?>
                                 <img width="100px" src="<?php echo base_url().''.$edit_game->game_img; ?>">
                              <?php
                           }
                           else
                           {
                              ?>
                                 <img width="100px" src="<?php echo base_url().'webroot/upload/dummy/user.png'; ?>">
                              <?php
                           }
                        ?>
                     </div>
                  </div> 
                  <div class="form-group col-md-2">
                     <div class="input text">
                        <label>&nbsp;</label>
                        <input data-validation="mime size" data-validation-allowing="jpg, png, gif, jpeg, jpe" data-validation-max-size="3M" name="game_img" type="file" id="game_img" value="" />
                        <small>Max upload size is 3MB</small>
                     </div>
                     <span class="text-danger" id="error_id"></span>
                  </div>
               </div>
               <!-- /.box-body -->      
               <div class="box-footer">
                  <button class="btn btn-success btn-sm" type="submit" name="Submit" value="Edit" >Submit</button>
                  <a class="btn btn-danger btn-sm" href="<?php echo base_url().MODULE_NAME;?>game">Cancel</a>
               </div>
         </form>
      </div>
      <!-- /.box -->
   </section>
   <!-- /.content -->
</aside>
<!-- /.right-side -->