<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Center Detail</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url().MODULE_NAME;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo base_url().MODULE_NAME;?>centerDetail">Center Detail</a></li>
            <li class="active">View Center Detail</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">       
        <div class="box box-success">
            <div class="box-header">
                <div class="pull-left">
                    <h3 class="box-title">View Center Detail</h3>
                </div>
                <div class="pull-right box-tools">
                    <a href="<?php echo base_url().MODULE_NAME;?>centerDetail" class="btn btn-info btn-sm">Back</a>
                </div>
            </div>
            <form action="" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                <?php  $csrf = array( 'name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash() ); ?>
                <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                <div class="box-body">
                    <div>
                        <div id="msg_div">
                            <?php echo $this->session->flashdata('message');?>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Center Name<span class="text-danger">*</span></label>
                                <input disabled name="center_name" id="center_name" class="form-control" type="text" value="<?php echo $edit_center_detail->center_name; ?>" />
                                <?php echo form_error('center_name','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Center Email<span class="text-danger">*</span></label>
                                <input disabled name="center_email" id="center_email" class="form-control" type="email" value="<?php echo $edit_center_detail->center_email; ?>" />
                                <?php echo form_error('center_email','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>                       
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Center Contact Number<span class="text-danger">*</span></label>
                                <input disabled name="center_contact_number" id="center_contact_number" class="form-control" type="text" value="<?php echo $edit_center_detail->center_contact_number; ?>" />
                                <?php echo form_error('center_contact_number','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>     
                    </div> 
                    <div class="row">
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Address</label>
                                <textarea disabled name="center_address" id="center_address" class="form-control"><?php echo $edit_center_detail->center_address; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Center Status<span class="text-danger">*</span></label>
                                <select disabled name="center_status" id="center_status" class="form-control">
                                    <option <?php if($edit_center_detail->center_status == '1'){ echo "selected"; } ?> value="1">Active</option>
                                    <option <?php if($edit_center_detail->center_status == '0'){ echo "selected"; } ?> value="0">Inactive</option>
                                </select>
                                <?php echo form_error('center_status','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Services<span class="text-danger">*</span></label>
                                <select disabled data-validation="required" name="services_id[]" id="services_id" class="form-control selectpicker" data-live-search="true" multiple>
                                <?php
                                    $services_id_arr = explode(',', $edit_center_detail->services_id);
                                    $services_res = $this->common_model->getData('tbl_services', array('services_status'=>'1'), 'multi');
                                    if(!empty($services_res))
                                    {
                                        foreach ($services_res as $s_val) 
                                        {
                                        ?>
                                            <option <?php echo (in_array($s_val->services_id, $services_id_arr)) ? 'selected' : ''; ?> value="<?php echo $s_val->services_id; ?>"><?php echo $s_val->services_name; ?></option>
                                        <?php
                                        }
                                    }
                                ?>
                                </select>
                            </div>
                        </div>
                    </div>  
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="input text">
                                <label>Description<span class="text-danger">*</span></label>
                                <span class="textarea_view"><?php echo $edit_center_detail->center_description; ?></span>
                            </div>
                        </div>
                    </div>  
                    <div class="row">               
                        <div class="form-group col-md-2">
                            <div class="input text">
                                <label>Center Logo<span class="text-danger">*</span></label><br>
                                <img src="<?php echo base_url().$edit_center_detail->center_logo; ?>" height='80'>
                            </div>
                        </div>                        
                    </div>  
                    <div class="row">                    
                        <div class="form-group col-md-12">
                            <label>Center images</label><br>
                            <span id="previewImg">
                                <?php
                                    $center_images = $this->common_model->getData('tbl_center_img', array('center_id'=>$edit_center_detail->center_id), 'multi');
                                    if(!empty($center_images)){
                                        foreach($center_images as $g_img) {
                                            ?>
                                            <div style='float:left;border:4px solid #303641;padding:5px;margin:5px;' id="gallery_img_<?php echo $g_img->center_img_id; ?>"><img height='80' src='<?php echo base_url().$g_img->center_img; ?>'></div>
                                            <?php
                                        }
                                    }
                                ?>
                            </span>
                        </div>                      
                    </div> 
                </div>
                <!-- /.box-body -->      
                <div class="box-footer">
                    <a class="btn btn-danger btn-sm" href="<?php echo base_url().MODULE_NAME;?>centerDetail">Cancel</a>
                </div>
            </form>
        </div>

        <!-- /.box -->
    </section>
    <!-- /.content -->
</aside>