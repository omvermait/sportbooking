<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Center Detail</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url().MODULE_NAME;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo base_url().MODULE_NAME;?>centerDetail">Center Detail</a></li>
            <li class="active">Create Center Detail</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">       
        <div class="box box-success">
            <div class="box-header">
                <div class="pull-left">
                    <h3 class="box-title">Create Center Detail</h3>
                </div>
                <div class="pull-right box-tools">
                    <a href="<?php echo base_url().MODULE_NAME;?>centerDetail" class="btn btn-info btn-sm">Back</a>                           
                </div>
            </div>
            <form action="" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                <?php  $csrf = array( 'name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash() ); ?>
                <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                <div class="box-body">
                    <div>
                        <div id="msg_div">
                            <?php echo $this->session->flashdata('message');?>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Center Name<span class="text-danger">*</span></label>
                                <input  data-validation="required" name="center_name" id="center_name" class="form-control" type="text" value="<?php echo set_value('center_name'); ?>" />
                                <?php echo form_error('center_name','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Center Email<span class="text-danger">*</span></label>
                                <input required name="center_email" id="center_email" class="form-control" type="email" value="<?php echo set_value('center_email'); ?>" />
                                <?php echo form_error('center_email','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>                      
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Center Contact Number<span class="text-danger">*</span></label>
                                <input required name="center_contact_number" id="center_contact_number" class="form-control" min="0" type="number" value="<?php echo set_value('center_contact_number'); ?>" />
                                <?php echo form_error('center_contact_number','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>   
                    </div> 
                    <div class="row">
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Address<span class="text-danger">*</span></label>
                                <textarea required name="center_address" id="center_address" class="form-control"><?php echo set_value('center_address'); ?></textarea>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Center Status<span class="text-danger">*</span></label>
                                <select required data-validation="required" name="center_status" id="center_status" class="form-control">
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                                <?php echo form_error('center_status','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Services<span class="text-danger">*</span></label>
                                <select data-validation="required" name="services_id[]" id="services_id" class="form-control selectpicker" data-live-search="true" multiple>
                                <?php
                                    $services_res = $this->common_model->getData('tbl_services', array('services_status'=>'1'), 'multi');
                                    if(!empty($services_res))
                                    {
                                        foreach ($services_res as $s_val) 
                                        {
                                            ?>
                                            <option value="<?php echo $s_val->services_id; ?>"><?php echo $s_val->services_name; ?></option>
                                            <?php
                                        }
                                    }
                                ?>
                                </select>
                                <?php echo form_error('services_id[]','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="input text">
                                <label>Description<span class="text-danger">*</span></label>
                                <textarea required rows="10" name="center_description" id="center_description" class="form-control tiny_textarea"><?php echo set_value('center_description'); ?></textarea>
                            </div>
                        </div>
                    </div>                      
                    <div class="row">                
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Center Logo<span class="text-danger">*</span></label>
                                <input type="file" required id="center_logo" name="center_logo">
                            </div>
                        </div>              
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Center Images<span class="text-danger">*</span></label>
                                <input type="file" required id="center_img" name="center_img[]" multiple="multiple">
                            </div>
                        </div>                     
                    </div>  
                    <div class="row">                    
                        <div class="form-group col-md-12">
                        <h3 id="previewImg_hadding" style="display: none;">Preview images</h3>
                            <span id="previewImg"></span>
                        </div>                      
                    </div>
                </div>
                <!-- /.box-body -->      
                <div class="box-footer">
                    <button class="btn btn-success btn-sm" type="submit" name="Submit" value="Add" >Submit</button>
                    <a class="btn btn-danger btn-sm" href="<?php echo base_url().MODULE_NAME;?>centerDetail">Cancel</a>
                </div>
            </form>
        </div>

        <!-- /.box -->
    </section>
    <!-- /.content -->
</aside>
<!-- /.right-side -->

<script type="text/javascript">
    $("#center_img").on("change", function (e) {
        $('#previewImg_hadding').css('display', 'block');
        var files = e.target.files,
        filesLength = files.length;      
        for (var i = 0; i < filesLength; i++) {
            var f = files[i];
            var fileReader = new FileReader();
            fileReader.onload = (function (e) {
                var file = e.target;
                var img = new Image();
                img.src = e.target.result;
                var res = null;
                img.onload = function() 
                {                    
                    $("#previewImg").append("<div style='float:left;border:4px solid #303641;padding:5px;margin:5px;'><img height='80' src='" + e.target.result + "'></div>").insertAfter("#previewImg");
                }
            });
            fileReader.readAsDataURL(f);
        }
    }); 
</script>