<!-- <?php
    $center_id_str = $this->data['session']->center_id;
    $find_set = '';
    if($center_id_str != '0')
    {
        $center_id = substr($center_id_str, 2, 10);
        $find_set = "FIND_IN_SET('".$center_id."', center_id)";
    }
?> -->
<aside class="right-side">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>Services</h1>
      <ol class="breadcrumb">
         <li><a href="<?php echo base_url().MODULE_NAME;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="<?php echo base_url().MODULE_NAME;?>services">Services</a></li>
         <li class="active">Create Services </li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="box box-success">
         <div class="box-header">
            <div class="pull-left">
               <h3 class="box-title">Create Services </h3>
            </div>
            <div class="pull-right box-tools">
               <a href="<?php echo base_url().MODULE_NAME;?>services" class="btn btn-info btn-sm">Back</a>
            </div>
         </div>
         <form action="" method="post" accept-charset="utf-8" enctype="multipart/form-data">
            <?php  $csrf = array( 'name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash() ); ?>
            <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
            <div class="box-body">
               <br>
               <div>
                  <div id="msg_div">
                     <?php echo $this->session->flashdata('message');?>
                  </div>
               </div>
              <!-- <div class="row">
                  <div class="form-group col-md-4">
                      <div class="input text">
                          <label>Center<span class="text-danger">*</span></label>
                          <?php
                              if($center_id_str == '0')
                              {
                                  ?>
                                  <select data-validation="required" name="center_id" id="center_id" class="selectpicker form-control" data-show-subtext="true" data-live-search="true">
                                      <option value="">-- Select --</option>
                                      <?php
                                          $center_list= $this->common_model->getData('tbl_center', array('center_status'=>'1'), 'multi');
                                          foreach ($center_list as $val) 
                                          {
                                              ?>
                                                 <option value="<?php echo $val->center_id; ?>"><?php echo $val->center_name; ?></option>
                                              <?php 
                                          }
                                      ?>
                                  </select>
                                  <?php
                              }
                              else
                              {
                                  $center_id_v = substr($center_id_str, 2, 10);
                                  $center_res = $this->common_model->getData('tbl_center', array('center_id'=>$center_id_v), 'single');
                                  ?>
                                  <input name="center_id" id="center_id" class="form-control" type="hidden" value="<?php echo !empty($center_res) ? $center_res->center_id : ''; ?>" />
                                  <input readonly class="form-control" type="text" value="<?php echo !empty($center_res) ? $center_res->center_name : ''; ?>" />
                                  <?php
                              }
                          ?>
                          <?php echo form_error('center_id','<span class="text-danger">','</span>'); ?>
                      </div>
                  </div>
              </div> -->
               <div class="row">
                  <div class="form-group col-md-4">
                     <div class="input text">
                        <label>Services Name<span class="text-danger">*</span></label>
                        <input required name="services_name" id="services_name" class="form-control" type="text" value="<?php echo set_value('services_name'); ?>" />
                        <?php echo form_error('services_name','<span class="text-danger">','</span>'); ?>
                     </div>
                  </div>
                  <div class="form-group col-md-4">
                     <div class="input text">
                        <label>Services Status<span class="text-danger">*</span></label>
                        <select data-validation="required" name="services_status" id="services_status" class="form-control">
                           <option value="1">Active</option>
                           <option value="0">Inactive</option>
                        </select>
                        <?php echo form_error('services_status','<span class="text-danger">','</span>'); ?>
                     </div>
                  </div>
                  <div class="form-group col-md-4">
                      <div class="input text">
                          <label>Services Image</label>
                          <input data-validation="mime size" data-validation-allowing="jpg, png, gif, jpeg, jpe" data-validation-max-size="3M" name="services_img" type="file" id="services_img" value="" />
                          <small>Max upload size is 3MB</small>
                      </div>
                      <span class="text-danger" id="error_id"></span>
                  </div>
               </div>
               <!-- /.box-body -->      
               <div class="box-footer">
                  <button class="btn btn-success btn-sm" type="submit" name="Submit" value="Add" >Submit</button>
                  <a class="btn btn-danger btn-sm" href="<?php echo base_url().MODULE_NAME;?>services">Cancel</a>
               </div>
         </form>
      </div>
      <!-- /.box -->
   </section>
   <!-- /.content -->
</aside>
<!-- /.right-side -->