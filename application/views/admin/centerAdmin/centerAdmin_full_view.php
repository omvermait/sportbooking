<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Center Admin</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url().MODULE_NAME;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo base_url().MODULE_NAME;?>centerAdmin">Center Admin</a></li>
            <li class="active">View Center Admin</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">       
        <div class="box box-success">
            <div class="box-header">
                <div class="pull-left">
                    <h3 class="box-title">View Center Admin</h3>
                </div>
                <div class="pull-right box-tools">
                    <a href="<?php echo base_url().MODULE_NAME;?>centerAdmin" class="btn btn-info btn-sm">Back</a>                           
                </div>
            </div>
            <?php
            foreach ($edit_user as $value) 
            {
                ?>
                    <form action="" method="post" accept-charset="utf-8" enctype="multipart/form-data" id="login_form">
                        <?php  $csrf = array( 'name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash() ); ?>
                        <input type="hidden" disabled name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                        <div class="box-body">
                            <div>
                                <div id="msg_div">
                                    <?php echo $this->session->flashdata('message');?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <div class="input text">
                                        <label>Center<span class="text-danger">*</span></label>
                                        <select disabled data-validation="required" name="center_id" id="center_id" class="selectpicker form-control" data-show-subtext="true" data-live-search="true">
                                            <?php
                                            $center_list= $this->common_model->getTableAllValue('tbl_center', 'center_status');
                                                foreach($center_list as $val) 
                                                {
                                                    ?>
                                                       <option <?php if($value->center_id == $val->center_id){ echo "selected"; } ?> value="<?php echo $val->center_id; ?>"><?php echo $val->center_name; ?></option>
                                                    <?php 
                                                }
                                            ?>
                                        </select>
                                        <?php echo form_error('center_id','<span class="text-danger">','</span>'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <div class="input text">
                                        <label>First Name</label>
                                        <input disabled name="user_fname" id="user_fname" class="form-control" type="text" value="<?php echo $value->user_fname; ?>" />
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <div class="input text">
                                        <label>Last Name</label>
                                        <input disabled name="user_lname" id="user_lname" class="form-control" type="text" value="<?php echo $value->user_lname; ?>" />
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <div class="input text">
                                        <label>Role</label>
                                        <select disabled name="role_id" id="role_id" class="form-control">
                                            <?php
                                                $role_list= $this->common_model->getTableAllValue('tbl_role', 'role_status'); 
                                                foreach ($role_list as $val) 
                                                {
                                                    if($val->role_id !='1')
                                                    {
                                                        ?>
                                                        <option <?php if($value->role_id == $val->role_id){ echo "selected"; } ?> value="<?php echo $val->role_id; ?>"><?php echo $val->role_name; ?></option>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <div class="input text">
                                        <label>Username</label>
                                        <input disabled name="user_name" id="user_name" class="form-control" type="text" value="<?php echo $value->user_name; ?>" />
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <div class="input text">
                                        <label>Phone Number</label>
                                        <input disabled name="user_mobile_no" id="user_mobile_no" class="form-control" type="number" min="0" value="<?php echo $value->user_mobile_no; ?>" />
                                    </div>
                                </div> 
                                <div class="form-group col-md-4">
                                    <div class="input text">
                                        <label>Email</label>
                                        <input disabled name="user_email" id="user_email" class="form-control" type="email" value="<?php echo $value->user_email; ?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <div class="input text">
                                        <label>Address</label>
                                        <textarea disabled name="user_address" id="user_address" class="form-control"><?php echo $value->user_address; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <div class="input text">
                                        <label>Status</label>
                                        <select disabled name="user_status" id="user_status" class="form-control">
                                            <option <?php if($value->user_status == '1'){ echo "selected"; } ?> value="1">Active</option>
                                            <option <?php if($value->user_status == '0'){ echo "selected"; } ?> value="0">Inactive</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <div class="input text">
                                        <label>Profile Image</label>
                                        <?php
                                            if(!empty($value->user_profile_img))
                                            {
                                                ?>
                                                <img width="100px" src="<?php echo base_url().''.$value->user_profile_img; ?>">
                                                <?php
                                            }
                                            else
                                            {
                                                ?>
                                                <img width="100px" src="<?php echo base_url().'webroot/upload/dummy/user.png'; ?>">
                                                <?php
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->      
                        <div class="box-footer">
                            <a class="btn btn-danger btn-sm" href="<?php echo base_url().MODULE_NAME;?>centerAdmin">Cancel</a>
                        </div>
                    </form>                    
                <?php
            }
            ?>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</aside>
<!-- /.right-side -->