<?php
    $center_id = $this->data['session']->center_id;
?>
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Center Admin</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url().MODULE_NAME;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo base_url().MODULE_NAME;?>centerAdmin">Center Admin</a></li>
            <li class="active">Create Center Admin</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">       
        <div class="box box-success">
            <div class="box-header">
                <div class="pull-left">
                    <h3 class="box-title">Create Center Admin</h3>
                </div>
                <div class="pull-right box-tools">
                    <a href="<?php echo base_url().MODULE_NAME;?>centerAdmin" class="btn btn-info btn-sm">Back</a>                           
                </div>
            </div>
            <form action="" id="login_form" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                <?php  $csrf = array( 'name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash() ); ?>
                <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                <div class="box-body">
                    <div>
                        <div id="msg_div">
                            <?php echo $this->session->flashdata('message');?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Center<span class="text-danger">*</span></label>
                                <?php
                                    if($center_id == '0')
                                    {
                                        ?>
                                        <select data-validation="required" name="center_id" id="center_id" class="selectpicker form-control" data-show-subtext="true" data-live-search="true">
                                            <option value="">-- Select --</option>
                                            <?php
                                                $center_list= $this->common_model->getTableAllValue('tbl_center', 'center_status');
                                                foreach ($center_list as $val) 
                                                {
                                                    ?>
                                                       <option value="<?php echo $val->center_id; ?>"><?php echo $val->center_name; ?></option>
                                                    <?php 
                                                }
                                            ?>
                                        </select>
                                        <?php
                                    }
                                    else
                                    {
                                        $center_res = $this->common_model->getTableValue('tbl_center', 'center_id', $center_id);
                                        ?>
                                        <input name="center_id" id="center_id" class="form-control" type="hidden" value="<?php echo !empty($center_res) ? $center_res->center_id : ''; ?>" />
                                        <input readonly class="form-control" type="text" value="<?php echo !empty($center_res) ? $center_res->center_name : ''; ?>" />
                                        <?php
                                    }
                                ?>
                                <?php echo form_error('center_id','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>First Name<span class="text-danger">*</span></label>
                                <input  data-validation="required" name="user_fname" id="user_fname" class="form-control" type="text" value="<?php echo set_value('user_fname'); ?>" />
                                <?php echo form_error('user_fname','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Last Name<span class="text-danger">*</span></label>
                                <input data-validation="required" name="user_lname" id="user_lname" class="form-control" type="text" value="<?php echo set_value('user_lname'); ?>" />
                                <?php echo form_error('user_lname','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Role<span class="text-danger">*</span></label>
                                <select data-validation="required" name="role_id" id="role_id" class="selectpicker form-control" data-show-subtext="true" data-live-search="true">
                                    <option value="">-- Select --</option>
                                    <?php
                                        $role_list= $this->common_model->getData('tbl_role', array('role_status'=>'1'), 'multi', $find_set);
                                        foreach ($role_list as $val) 
                                        {
                                            if($center_id_str == '0')
                                            {
                                                if($val->role_id != '1')
                                                {
                                                    ?>
                                                       <option value="<?php echo $val->role_id; ?>"><?php echo $val->role_name; ?></option>
                                                    <?php 
                                                }  
                                            }
                                            else
                                            {
                                                if($val->role_id > '2')
                                                {
                                                    ?>
                                                       <option value="<?php echo $val->role_id; ?>"><?php echo $val->role_name; ?></option>
                                                    <?php 
                                                }                                                
                                            }
                                        }
                                    ?>
                                </select>
                                <?php echo form_error('role_id','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                    </div>  
                    <div class="row">
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Username<span class="text-danger">*</span></label>
                                <input required data-validation="custom" data-validation-regexp="^([a-zA-z -]+)$" data-validation-allowing="- _" name="user_name" id="user_name" class="form-control" type="text" value="<?php echo set_value('user_name'); ?>" />
                                <?php echo form_error('user_name','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Password<span class="text-danger">*</span></label>
                                <input data-validation="required" name="user_password" id="user_password" class="form-control" type="password" value="" />
                                <?php echo form_error('user_password','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Confirm Password<span class="text-danger">*</span></label>
                                <input required="required" data-validation="confirmation" data-validation-confirm="user_password" name="user_cpassword" id="user_cpassword" class="form-control" type="password" value="" />
                                <?php echo form_error('user_cpassword','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Email<span class="text-danger">*</span></label>
                                <input required="required" data-validation="email" name="user_email" id="user_email" class="form-control" type="email" value="<?php echo set_value('user_email'); ?>" />
                                <?php echo form_error('user_email','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Phone Number<span class="text-danger">*</span></label>
                                <input required="required" data-validation="number length" data-validation-length="10"  data-validation-error-msg="Phone no. has to be 10 chars" name="user_mobile_no" id="user_mobile_no" class="form-control" type="number" min="0" value="<?php echo set_value('user_mobile_no'); ?>" />
                                <?php echo form_error('user_mobile_no','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Address<span class="text-danger">*</span></label>
                                <textarea data-validation="required" name="user_address" id="user_address" class="form-control"><?php echo set_value('user_address'); ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Status<span class="text-danger">*</span></label>
                                <select data-validation="required" name="user_status" id="user_status" class="form-control">
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Profile Image</label>
                                <input data-validation="mime size required" data-validation-allowing="jpg, png, gif, jpeg, jpe" data-validation-max-size="3M" name="user_profile_img" type="file" id="user_profile_img" value="" />
                                <small>Max upload size is 3MB</small>
                            </div>
                            <span class="text-danger" id="error_id"></span>
                        </div>  
                    </div>
                </div>
                <!-- /.box-body -->      
                <div class="box-footer">
                    <button class="btn btn-success btn-sm" type="submit" name="Submit" value="Add" >Submit</button>
                    <a class="btn btn-danger btn-sm" href="<?php echo base_url().MODULE_NAME;?>centerAdmin">Cancel</a>
                </div>
            </form>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</aside>
<!-- /.right-side -->
<script type="text/javascript">
    $("#login_form").submit(function() 
    {
        if(!checkFiletype())
        {
            return false;
        }
        var user_password = $('[name=user_password]').val();
        if(user_password)
        {
            $('[name=user_password]').val(sha256(user_password));
        }
        var user_cpassword = $('[name=user_cpassword]').val();
        if(user_cpassword)
        {
            $('[name=user_cpassword]').val(sha256(user_cpassword));
        }

    });

    function checkFiletype()
    {      
        if($('#user_profile_img').val() == '')
        {
            return true;
        }
        var filename = $('#user_profile_img').val();
        var extension = filename.replace(/^.*\./, '');
        extension = extension.toLowerCase();
        if(extension == 'png' || extension == 'gif' || extension == 'jpe' || extension == 'jpe' || extension == 'jpeg' || extension == 'jpg')
        {  
            $('#error_id').html("");
            return true;             
        }
        else
        {
            $('#user_profile_img').val('');
            $('#error_id').html("<p></p>Invalid file type please choose only image file!");
            return false; 
        }
    }
</script>