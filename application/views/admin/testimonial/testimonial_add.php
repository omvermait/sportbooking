<aside class="right-side">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>Testimonial</h1>
      <ol class="breadcrumb">
         <li><a href="<?php echo base_url().MODULE_NAME;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="<?php echo base_url().MODULE_NAME;?>testimonial">Testimonial</a></li>
         <li class="active">Create Testimonial </li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="box box-success">
         <div class="box-header">
            <div class="pull-left">
               <h3 class="box-title">Create Testimonial </h3>
            </div>
            <div class="pull-right box-tools">
               <a href="<?php echo base_url().MODULE_NAME;?>testimonial" class="btn btn-info btn-sm">Back</a>
            </div>
         </div>
         <form action="" method="post" accept-charset="utf-8" enctype="multipart/form-data">
            <?php  $csrf = array( 'name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash() ); ?>
            <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
            <div class="box-body">
               <br>
               <div>
                  <div id="msg_div">
                     <?php echo $this->session->flashdata('message');?>
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-md-4">
                     <div class="input text">
                        <label>Testimonial Name<span class="text-danger">*</span></label>
                        <input required data-validation="alphanumeric" data-validation-allowing="- _" name="testimonial_name" id="testimonial_name" class="form-control" type="text" value="<?php echo set_value('testimonial_name'); ?>" />
                        <?php echo form_error('testimonial_name','<span class="text-danger">','</span>'); ?>
                     </div>
                  </div>
                  <div class="form-group col-md-4">
                     <div class="input text">
                        <label>Testimonial Status<span class="text-danger">*</span></label>
                        <select data-validation="required" name="testimonial_status" id="testimonial_status" class="form-control">
                           <option value="1">Active</option>
                           <option value="0">Inactive</option>
                        </select>
                        <?php echo form_error('testimonial_status','<span class="text-danger">','</span>'); ?>
                     </div>
                  </div>
                  <div class="form-group col-md-4">
                      <div class="input text">
                          <label>Testimonial Image</label>
                          <input data-validation="mime size" data-validation-allowing="jpg, png, gif, jpeg, jpe" data-validation-max-size="3M" name="testimonial_img" type="file" id="testimonial_img" value="" />
                          <small>Max upload size is 3MB</small>
                      </div>
                      <span class="text-danger" id="error_id"></span>
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-md-12">
                     <div class="input text">
                        <label>Testimonial Title<span class="text-danger">*</span></label>
                        <textarea name="testimonial_title" id="testimonial_title" class="form-control"></textarea>
                        <?php echo form_error('testimonial_title','<span class="text-danger">','</span>'); ?>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-md-12">
                     <div class="input text">
                        <label>Testimonial Description<span class="text-danger">*</span></label>
                        <textarea name="testimonial_description" id="testimonial_description" class="form-control tiny_textarea"></textarea>
                        <?php echo form_error('testimonial_description','<span class="text-danger">','</span>'); ?>
                     </div>
                  </div>
               </div>
               <!-- /.box-body -->      
               <div class="box-footer">
                  <button class="btn btn-success btn-sm" type="submit" name="Submit" value="Add" >Submit</button>
                  <a class="btn btn-danger btn-sm" href="<?php echo base_url().MODULE_NAME;?>testimonial">Cancel</a>
               </div>
         </form>
      </div>
      <!-- /.box -->
   </section>
   <!-- /.content -->
</aside>
<!-- /.right-side -->