<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Ground</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url().MODULE_NAME;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo base_url().MODULE_NAME;?>ground">Ground</a></li>
            <li class="active">View Ground </li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box box-success">
            <div class="box-header">
                <div class="pull-left">
                    <h3 class="box-title">View Ground </h3>
                </div>
                <div class="pull-right box-tools">
                    <a href="<?php echo base_url().MODULE_NAME;?>ground" class="btn btn-info btn-sm">Back</a>
                </div>
            </div>
            <form action="" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                <?php  $csrf = array( 'name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash() ); ?>
                <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                <div class="box-body">
                    <br>
                    <div>
                        <div id="msg_div">
                            <?php echo $this->session->flashdata('message');?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Center<span class="text-danger">*</span></label>
                                <select disabled data-validation="required" name="center_id" id="center_id" class="selectpicker form-control" data-show-subtext="true" data-live-search="true">
                                    <?php
                                    $center_list= $this->common_model->getData('tbl_center', array('center_status'=>'1'), 'multi');
                                        foreach($center_list as $val) 
                                        {
                                            ?>
                                               <option <?php if($edit_ground->center_id == $val->center_id){ echo "selected"; } ?> value="<?php echo $val->center_id; ?>"><?php echo $val->center_name; ?></option>
                                            <?php 
                                        }
                                    ?>
                                </select>
                                <?php echo form_error('center_id','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Ground Title<span class="text-danger">*</span></label>
                                <input disabled name="ground_name" id="ground_name" class="form-control" type="text" value="<?php echo $edit_ground->ground_name; ?>" />
                                <?php echo form_error('ground_name','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Ground Status<span class="text-danger">*</span></label>
                                <select disabled data-validation="required" name="ground_status" id="ground_status" class="form-control">
                                    <option <?php echo ($edit_ground->ground_status == '1') ? 'selected' : ''; ?> value="1">Active</option>
                                    <option <?php echo ($edit_ground->ground_status == '0') ? 'selected' : ''; ?> value="0">Inactive</option>
                                </select>
                                <?php echo form_error('ground_status','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Facility<span class="text-danger">*</span></label>
                                <select disabled data-validation="required" name="facility_id" id="facility_id" class="form-control selectpicker" data-live-search="true">
                                <?php
                                    $facility_id_arr = explode(',', $edit_ground->facility_id);
                                    $facility_res = $this->common_model->getData('tbl_facility', array('facility_status'=>'1'), 'multi');
                                    if(!empty($facility_res))
                                    {
                                        foreach ($facility_res as $s_val) 
                                        {
                                        ?>
                                            <option <?php echo (in_array($s_val->facility_id, $facility_id_arr)) ? 'selected' : ''; ?> value="<?php echo $s_val->facility_id; ?>"><?php echo $s_val->facility_name; ?></option>
                                        <?php
                                        }
                                    }
                                ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Game<span class="text-danger">*</span></label>
                                <select disabled data-validation="required" name="game_id" id="game_id" class="form-control selectpicker" data-live-search="true">
                                <?php
                                    $game_res = $this->common_model->getData('tbl_game', array('game_status'=>'1'), 'multi');
                                    if(!empty($game_res))
                                    {
                                        foreach ($game_res as $s_val) 
                                        {
                                            ?>
                                            <option <?php echo ($edit_ground->game_id == $s_val->game_id) ? 'selected' : ''; ?> value="<?php echo $s_val->game_id; ?>"><?php echo $s_val->game_name; ?></option>
                                            <?php
                                        }
                                    }
                                ?>
                                </select>
                            </div>
                        </div><!-- 
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Services<span class="text-danger">*</span></label>
                                <select disabled data-validation="required" name="services_id[]" id="services_id" class="form-control selectpicker" data-live-search="true" multiple>
                                <?php
                                    $services_id_arr = explode(',', $edit_ground->services_id);
                                    $services_res = $this->common_model->getData('tbl_services', array('services_status'=>'1'), 'multi');
                                    if(!empty($services_res))
                                    {
                                        foreach ($services_res as $s_val) 
                                        {
                                        ?>
                                            <option <?php echo (in_array($s_val->services_id, $services_id_arr)) ? 'selected' : ''; ?> value="<?php echo $s_val->services_id; ?>"><?php echo $s_val->services_name; ?></option>
                                        <?php
                                        }
                                    }
                                ?>
                                </select>
                            </div>
                        </div> -->
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Ground Size(In Square Feet)<span class="text-danger">*</span></label>
                                <input disabled required name="ground_size" id="ground_size" class="form-control" type="text" value="<?php echo $edit_ground->ground_size; ?>" />
                                <?php echo form_error('ground_size','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Ground Rent Price<span class="text-danger">*</span></label>
                                <input disabled required name="ground_price" id="ground_price" class="form-control" type="text" value="<?php echo $edit_ground->ground_price; ?>" />
                                <?php echo form_error('ground_price','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row"> 
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Address<span class="text-danger">*</span></label>
                                <textarea disabled data-validation="required" name="ground_address" id="ground_address" class="form-control"><?php echo $edit_ground->ground_address; ?></textarea>
                                <?php echo form_error('ground_address','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>  
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Available Time<span class="text-danger">*</span></label>
                                <select disabled data-validation="required" name="time_id[]" id="time_id" class="form-control selectpicker" data-live-search="true" multiple>
                                <?php
                                    $time_slot_res = $this->common_model->getData('tbl_time_slot', array('time_status'=>'1'), 'multi');
                                    if(!empty($time_slot_res))
                                    {
                                        foreach ($time_slot_res as $s_val) 
                                        {
                                            $check_time_slot = $this->common_model->getData('tbl_ground_time_slot', array('time_id'=>$s_val->time_id, 'ground_id'=>$edit_ground->ground_id, 'ground_time_slot_status'=>'1'), 'single');   
                                            ?>
                                            <option <?php echo (!empty($check_time_slot)) ? 'selected' : ''; ?> value="<?php echo $s_val->time_id; ?>"><?php echo $s_val->start_time.' - '.$s_val->end_time; ?></option>
                                            <?php
                                        }
                                    }
                                ?>
                                </select>
                                <?php echo form_error('time_id','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>  
                    </div>
                    <div class="row">  
                        <div class="form-group col-md-2">
                            <div class="input text">
                                <label>Thumbnail Image<span class="text-danger">*</span></label>
                                <img src="<?php echo base_url().$edit_ground->ground_img; ?>" height="80">
                            </div>
                        </div>  
                    </div>
                    <div class="row">                    
                        <div class="form-group col-md-12">
                            <label>Ground Images</label><br>
                            <span id="previewImg">
                                <?php
                                    $ground_images = $this->common_model->getData('tbl_ground_img', NULL, 'multi');
                                    if(!empty($ground_images)){
                                        foreach($ground_images as $g_img) {
                                            ?>
                                            <div style='float:left;border:4px solid #303641;padding:5px;margin:5px;' id="gallery_img_<?php echo $g_img->ground_img_id; ?>"><img height='80' src='<?php echo base_url().$g_img->ground_img; ?>'></div>
                                            <?php
                                        }
                                    }
                                ?>
                            </span>
                        </div>                      
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="input text">
                                <label>Ground Desciption</label>
                                <span class="textarea_view"><?php echo $edit_ground->ground_description; ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a class="btn btn-danger btn-sm" href="<?php echo base_url().MODULE_NAME;?>ground">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</aside>