<?php
    $center_id = $this->data['session']->center_id;
?>
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Ground</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url().MODULE_NAME;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo base_url().MODULE_NAME;?>ground">Ground</a></li>
            <li class="active">Create Ground </li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box box-success">
            <div class="box-header">
                <div class="pull-left">
                    <h3 class="box-title">Create Ground </h3>
                </div>
                <div class="pull-right box-tools">
                    <a href="<?php echo base_url().MODULE_NAME;?>ground" class="btn btn-info btn-sm">Back</a>
                </div>
            </div>
            <form action="" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                <?php  $csrf = array( 'name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash() ); ?>
                <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                <div class="box-body">
                    <br>
                    <div>
                        <div id="msg_div">
                            <?php echo $this->session->flashdata('message');?>
                        </div>
                    </div>
                  <div class="row">
                      <div class="form-group col-md-4">
                          <div class="input text">
                              <label>Center<span class="text-danger">*</span></label>
                              <?php
                                  if($center_id == '0')
                                  {
                                      ?>
                                      <select data-validation="required" name="center_id" id="center_id" class="selectpicker form-control" data-show-subtext="true" data-live-search="true">
                                          <option value="">-- Select --</option>
                                          <?php
                                              $center_list= $this->common_model->getData('tbl_center', array('center_status'=>'1'), 'multi');
                                              foreach ($center_list as $val) 
                                              {
                                                  ?>
                                                     <option value="<?php echo $val->center_id; ?>"><?php echo $val->center_name; ?></option>
                                                  <?php 
                                              }
                                          ?>
                                      </select>
                                      <?php
                                  }
                                  else
                                  {
                                      $center_res = $this->common_model->getData('tbl_center', array('center_id'=>$center_id), 'single');
                                      ?>
                                      <input name="center_id" id="center_id" class="form-control" type="hidden" value="<?php echo !empty($center_res) ? $center_res->center_id : ''; ?>" />
                                      <input readonly class="form-control" type="text" value="<?php echo !empty($center_res) ? $center_res->center_name : ''; ?>" />
                                      <?php
                                  }
                              ?>
                              <?php echo form_error('center_id','<span class="text-danger">','</span>'); ?>
                          </div>
                      </div>
                  </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Ground Title<span class="text-danger">*</span></label>
                                <input required name="ground_name" id="ground_name" class="form-control" type="text" value="<?php echo set_value('ground_name'); ?>" />
                                <?php echo form_error('ground_name','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Ground Status<span class="text-danger">*</span></label>
                                <select data-validation="required" name="ground_status" id="ground_status" class="form-control">
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                                <?php echo form_error('ground_status','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Facility<span class="text-danger">*</span></label>
                                <select data-validation="required" name="facility_id[]" id="facility_id" class="form-control selectpicker" data-live-search="true" multiple>
                                <?php
                                    $facility_res = $this->common_model->getData('tbl_facility', array('facility_status'=>'1'), 'multi');
                                    if(!empty($facility_res))
                                    {
                                        foreach ($facility_res as $s_val) 
                                        {
                                            ?>
                                            <option value="<?php echo $s_val->facility_id; ?>"><?php echo $s_val->facility_name; ?></option>
                                            <?php
                                        }
                                    }
                                ?>
                                </select>
                                <?php echo form_error('quality_id','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Game<span class="text-danger">*</span></label>
                                <select data-validation="required" name="game_id" id="game_id" class="form-control selectpicker" data-live-search="true">
                                    <option value="">-- Select --</option>
                                <?php
                                    $game_res = $this->common_model->getData('tbl_game', array('game_status'=>'1'), 'multi');
                                    if(!empty($game_res))
                                    {
                                        foreach ($game_res as $s_val) 
                                        {
                                            ?>
                                            <option value="<?php echo $s_val->game_id; ?>"><?php echo $s_val->game_name; ?></option>
                                            <?php
                                        }
                                    }
                                ?>
                                </select>
                                <?php echo form_error('game_id','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div><!-- 
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Services<span class="text-danger">*</span></label>
                                <select data-validation="required" name="services_id[]" id="services_id" class="form-control selectpicker" data-live-search="true" multiple>
                                <?php
                                    $services_res = $this->common_model->getData('tbl_services', array('services_status'=>'1'), 'multi');
                                    if(!empty($services_res))
                                    {
                                        foreach ($services_res as $s_val) 
                                        {
                                            ?>
                                            <option value="<?php echo $s_val->services_id; ?>"><?php echo $s_val->services_name; ?></option>
                                            <?php
                                        }
                                    }
                                ?>
                                </select>
                                <?php echo form_error('services_id[]','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div> -->
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Ground Size(In Square Feet)<span class="text-danger">*</span></label>
                                <input required name="ground_size" id="ground_size" class="form-control" type="text" value="<?php echo set_value('ground_size'); ?>" />
                                <?php echo form_error('ground_size','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Ground Rent Price<span class="text-danger">*</span></label>
                                <input required name="ground_price" id="ground_price" class="form-control" type="text" value="<?php echo set_value('ground_price'); ?>" />
                                <?php echo form_error('ground_price','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Address<span class="text-danger">*</span></label>
                                <textarea data-validation="required" name="ground_address" id="ground_address" class="form-control"><?php echo set_value('ground_address'); ?></textarea>
                                <?php echo form_error('ground_address','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>     
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Available Time<span class="text-danger">*</span></label>
                                <select data-validation="required" name="time_id[]" id="time_id" class="form-control selectpicker" data-live-search="true" multiple>
                                <?php
                                    $time_slot_res = $this->common_model->getData('tbl_time_slot', array('time_status'=>'1'), 'multi');
                                    if(!empty($time_slot_res))
                                    {
                                        foreach ($time_slot_res as $s_val) 
                                        {
                                            ?>
                                            <option value="<?php echo $s_val->time_id; ?>"><?php echo $s_val->start_time.' - '.$s_val->end_time; ?></option>
                                            <?php
                                        }
                                    }
                                ?>
                                </select>
                                <?php echo form_error('time_id[]','<span class="text-danger">','</span>'); ?>
                            </div>
                        </div>                   
                    </div>  
                    <div class="row">                
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Thumbnail Image<span class="text-danger">*</span></label>
                                <input type="file" required id="ground_img" name="ground_img" >
                            </div>
                        </div>                
                        <div class="form-group col-md-4">
                            <div class="input text">
                                <label>Ground Images<span class="text-danger">*</span></label>
                                <input type="file" required id="ground_img_m" name="ground_img_m[]" multiple="multiple">
                            </div>
                        </div>                     
                    </div>  
                    <div class="row">                    
                        <div class="form-group col-md-12">
                        <h3 id="previewImg_hadding" style="display: none;">Preview images</h3>
                            <span id="previewImg"></span>
                        </div>                      
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="input text">
                                <label>Ground Desciption</label>
                                <textarea data-validation="required" rows="10" name="ground_description" id="ground_description" class="form-control tiny_textarea"></textarea>
                                <?php echo form_error('ground_description','<span class="text-danger">','</span>'); ?>
                            </div>
                            <span class="text-danger" id="error_id"></span>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button class="btn btn-success btn-sm" type="submit" name="Submit" value="Add" >Submit</button>
                        <a class="btn btn-danger btn-sm" href="<?php echo base_url().MODULE_NAME;?>ground">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</aside>
<script type="text/javascript">
    $("#ground_img_m").on("change", function (e) {
        $('#previewImg_hadding').css('display', 'block');
        var files = e.target.files,
        filesLength = files.length;      
        for (var i = 0; i < filesLength; i++) {
            var f = files[i];
            var fileReader = new FileReader();
            fileReader.onload = (function (e) {
                var file = e.target;
                var img = new Image();
                img.src = e.target.result;
                var res = null;
                img.onload = function() 
                {                    
                    $("#previewImg").append("<div style='float:left;border:4px solid #303641;padding:5px;margin:5px;'><img height='80' src='" + e.target.result + "'></div>").insertAfter("#previewImg");
                }
            });
            fileReader.readAsDataURL(f);
        }
    }); 
</script>