<aside class="right-side">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>FAQ</h1>
      <ol class="breadcrumb">
         <li><a href="<?php echo base_url().MODULE_NAME;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="<?php echo base_url().MODULE_NAME;?>faq">FAQ</a></li>
         <li class="active">View FAQ </li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="box box-success">
         <div class="box-header">
            <div class="pull-left">
               <h3 class="box-title">View FAQ </h3>
            </div>
            <div class="pull-right box-tools">
               <a href="<?php echo base_url().MODULE_NAME;?>faq" class="btn btn-info btn-sm">Back</a>
            </div>
         </div>
         <form action="" method="post" accept-charset="utf-8" enctype="multipart/form-data">
            <?php  $csrf = array( 'name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash() ); ?>
            <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
            <div class="box-body">
               <br>
               <div>
                  <div id="msg_div">
                     <?php echo $this->session->flashdata('message');?>
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-md-8">
                     <div class="input text">
                        <label>FAQ Title<span class="text-danger">*</span></label><br>
                        <span class="textarea_view"><?php echo $edit_faq->faq_title; ?></span>
                        <?php echo form_error('faq_title','<span class="text-danger">','</span>'); ?>
                     </div>
                  </div>
                  <div class="form-group col-md-4">
                     <div class="input text">
                        <label>FAQ Status<span class="text-danger">*</span></label>
                        <select disabled data-validation="required" name="faq_status" id="faq_status" class="form-control">
                           <option <?php echo ($edit_faq->faq_status == 1) ? 'selected' : ''; ?> value="1">Active</option>
                           <option <?php echo ($edit_faq->faq_status == 0) ? 'selected' : ''; ?> value="0">Inactive</option>
                        </select>
                        <?php echo form_error('faq_status','<span class="text-danger">','</span>'); ?>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-md-12">
                     <div class="input text">
                        <label>FAQ Description<span class="text-danger">*</span></label>
                        <span class="textarea_view"><?php echo $edit_faq->faq_description; ?></span>
                        <?php echo form_error('faq_description','<span class="text-danger">','</span>'); ?>
                     </div>
                  </div>
               </div>
               <!-- /.box-body -->      
               <div class="box-footer">
                  <a class="btn btn-danger btn-sm" href="<?php echo base_url().MODULE_NAME;?>faq">Cancel</a>
               </div>
         </form>
      </div>
      <!-- /.box -->
   </section>
   <!-- /.content -->
</aside>
<!-- /.right-side -->