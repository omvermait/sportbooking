<aside class="right-side">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>Facility</h1>
      <ol class="breadcrumb">
         <li><a href="<?php echo base_url().MODULE_NAME;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="<?php echo base_url().MODULE_NAME;?>facility">Facility</a></li>
         <li class="active">Create Facility </li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="box box-success">
         <div class="box-header">
            <div class="pull-left">
               <h3 class="box-title">Create Facility </h3>
            </div>
            <div class="pull-right box-tools">
               <a href="<?php echo base_url().MODULE_NAME;?>facility" class="btn btn-info btn-sm">Back</a>
            </div>
         </div>
         <form action="" method="post" accept-charset="utf-8" enctype="multipart/form-data">
            <?php  $csrf = array( 'name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash() ); ?>
            <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
            <div class="box-body">
               <br>
               <div>
                  <div id="msg_div">
                     <?php echo $this->session->flashdata('message');?>
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-md-4">
                     <div class="input text">
                        <label>Facility Name<span class="text-danger">*</span></label>
                        <input required data-validation="alphanumeric" data-validation-allowing="- _" name="facility_name" id="facility_name" class="form-control" type="text" value="<?php echo set_value('facility_name'); ?>" />
                        <?php echo form_error('facility_name','<span class="text-danger">','</span>'); ?>
                     </div>
                  </div>
                  <div class="form-group col-md-4">
                     <div class="input text">
                        <label>Facility Status<span class="text-danger">*</span></label>
                        <select data-validation="required" name="facility_status" id="facility_status" class="form-control">
                           <option value="1">Active</option>
                           <option value="0">Inactive</option>
                        </select>
                        <?php echo form_error('facility_status','<span class="text-danger">','</span>'); ?>
                     </div>
                  </div>
                  <div class="form-group col-md-4">
                      <div class="input text">
                          <label>Facility Image</label>
                          <input data-validation="mime size" data-validation-allowing="jpg, png, gif, jpeg, jpe" data-validation-max-size="3M" name="facility_img" type="file" id="facility_img" value="" />
                          <small>Max upload size is 3MB</small>
                      </div>
                      <span class="text-danger" id="error_id"></span>
                  </div>
               </div>
               <!-- /.box-body -->      
               <div class="box-footer">
                  <button class="btn btn-success btn-sm" type="submit" name="Submit" value="Add" >Submit</button>
                  <a class="btn btn-danger btn-sm" href="<?php echo base_url().MODULE_NAME;?>facility">Cancel</a>
               </div>
         </form>
      </div>
      <!-- /.box -->
   </section>
   <!-- /.content -->
</aside>
<!-- /.right-side -->