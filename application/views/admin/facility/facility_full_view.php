<aside class="right-side">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>Facility</h1>
      <ol class="breadcrumb">
         <li><a href="<?php echo base_url().MODULE_NAME;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="<?php echo base_url().MODULE_NAME;?>facility">Facility</a></li>
         <li class="active">View Facility </li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="box box-success">
         <div class="box-header">
            <div class="pull-left">
               <h3 class="box-title">View Facility </h3>
            </div>
            <div class="pull-right box-tools">
               <a href="<?php echo base_url().MODULE_NAME;?>facility" class="btn btn-info btn-sm">Back</a>
            </div>
         </div>
         <form action="" method="post" accept-charset="utf-8" enctype="multipart/form-data">
            <?php  $csrf = array( 'name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash() ); ?>
            <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
            <div class="box-body">
               <br>
               <div>
                  <div id="msg_div">
                     <?php echo $this->session->flashdata('message');?>
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-md-4">
                     <div class="input text">
                        <label>Facility Name<span class="text-danger">*</span></label>
                        <input disabled required data-validation="alphanumeric" data-validation-allowing="- _" name="facility_name" id="facility_name" class="form-control" type="text" value="<?php echo $edit_facility->facility_name; ?>" />
                        <?php echo form_error('facility_name','<span class="text-danger">','</span>'); ?>
                     </div>
                  </div>
                  <div class="form-group col-md-4">
                     <div class="input text">
                        <label>Facility Status<span class="text-danger">*</span></label>
                        <select disabled data-validation="required" name="facility_status" id="facility_status" class="form-control">
                           <option <?php echo ($edit_facility->facility_status == 1) ? 'selected' : ''; ?> value="1">Active</option>
                           <option <?php echo ($edit_facility->facility_status == 0) ? 'selected' : ''; ?> value="0">Inactive</option>
                        </select>
                        <?php echo form_error('facility_status','<span class="text-danger">','</span>'); ?>
                     </div>
                  </div>
                    <div class="form-group col-md-2">
                        <div class="input text">
                            <label>Facility Image</label>
                            <?php
                                if(!empty($edit_facility->facility_img))
                                {
                                    ?>
                                    <img width="100px" src="<?php echo base_url().''.$edit_facility->facility_img; ?>">
                                    <?php
                                }
                                else
                                {
                                    ?>
                                    <img width="100px" src="<?php echo base_url().'webroot/upload/dummy/user.png'; ?>">
                                    <?php
                                }
                            ?>
                        </div>
                    </div> 
               </div>
               <!-- /.box-body -->      
               <div class="box-footer">
                  <a class="btn btn-danger btn-sm" href="<?php echo base_url().MODULE_NAME;?>facility">Cancel</a>
               </div>
         </form>
      </div>
      <!-- /.box -->
   </section>
   <!-- /.content -->
</aside>
<!-- /.right-side -->