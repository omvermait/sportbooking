<div class="login_section">
	<div class="container">
		<div class="row">
			<div class="login_regstr">
				<div class="login_pg_login">
					<h1 class="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".3s">Sign In</h1>
					<a class="fb_login_link" href="#"><img src="<?php echo base_url(); ?>webroot/front/assets/images/login_fb.png">LOGIN WITH FACEBOOK</a>
					<a href=""><img src="<?php echo base_url(); ?>webroot/front/assets/images/google-logo.png">SIGN IN WITH GOOGLE</a>
					<h5>Or with the classic login</h5>
					<div>
	                    <div id="msg_div">
	                        <?php echo $this->session->flashdata('message');?>
	                    </div>
	                </div> 
					<form action="" method="post">
					  	<label for="login_name"></label><br>
					  	<input type="text" id="login_name" name="login_name" placeholder="Email" onkeyup="errorremove('err_login_name')"><br>
                        <?php echo form_error('login_name','<span class="text-danger" id="err_login_name">','</span>'); ?>
					  	<label for="customer_password"></label><br>
					  	<input type="password" id="customer_password" name="customer_password" placeholder="Password" onkeyup="errorremove('err_customer_password')"><br><br>
                        <?php echo form_error('customer_password','<span class="text-danger" id="err_customer_password">','</span>'); ?>
						<button type="submit" name="Submit" id="Submit" value="Submit" class="wow bounceIn sign_in_btn" data-wow-duration="1s" data-wow-delay=".3s">Sign In</button>
					</form>
					<p>
						Forgot your password or don't have one yet?<a class="click_here_btn" href="#">Click here to retrieve it</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</div> 