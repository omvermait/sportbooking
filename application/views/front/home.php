
        <div class="row">
            <div class="banner_area">
                <div class="banner_content">
                    <h1>Trova il centro sportivo più vicino a te<br>
                        PRENOTARE CON NOI CONVIENE, SEMPRE
                    </h1>
                </div>
                <div class="search_area">
                    <div class="row">
                        <div class="banner_search_content">
                            <div class="col-md-4">
                                <div class="location">
                                    <label for="ground_address"><img src="<?php echo base_url(); ?>webroot/front/assets/images/icon1-location.png"></label>
                                    <input type="text" id="ground_address" name="ground_address" value="" placeholder="Citta, Indirizzo, centro Sport">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="sport">
                                    <label for="game_id"><img src="<?php echo base_url(); ?>webroot/front/assets/images/icon-globe.png"></label>
                                    <select name="game_id" id="game_id">
                                        <?php
                                            $game_res = $this->common_model->getData('tbl_game', array('game_status'=>'1'), 'multi');
                                            if(!empty($game_res)){
                                                foreach ($game_res as $g_val) {
                                                    ?>
                                                    <option value="<?php echo $g_val->game_id; ?>"><?php echo $g_val->game_name; ?></option>
                                                    <?php
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="calender">
                                    <label for="booking_date"><img src="<?php echo base_url(); ?>webroot/front/assets/images/icon-calendar.png"></label>
                                    <input type="date" id="booking_date" name="booking_date">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="clock">
                                    <label for="booking_time"><img src="<?php echo base_url(); ?>webroot/front/assets/images/icon-time.png"></label>
                                    <select name="booking_time" id="booking_time">
                                        <?php
                                            $time_slot_res = $this->common_model->getData('tbl_time_slot', array('time_status'=>'1'), 'multi');
                                            if(!empty($time_slot_res)){
                                                foreach ($time_slot_res as $t_val) {
                                                    ?>
                                                    <option value="<?php echo $t_val->time_id; ?>"><?php echo $t_val->start_time; ?></option>
                                                    <?php
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wow fadeInDown find_space" data-wow-duration="1s" data-wow-delay=".3s">
                    <h1><a href="#"> FIND SPACE</a></h1>
                </div>
            </div>
        </div>
        <div class="row section1">
            <div class="container">
                <div class="row">
                    <div class="featured_offer">
                        <div class="col-md-6">
                            <div class="wow fadeInDown featured_col1"data-wow-duration="1s" data-wow-delay=".3s">
                                <h2>Offerte In Evidenza</h2>
                                <p>Non lasciarti scappare le promozioni esclusive dei nostri partner con campi scontati <b>fino al 50%!</b> Qualità, convenienza e divertimento, ogni giorno</p>
                                <a href="#">Cerca</a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="wow fadeInDown featured_col1"data-wow-duration="1s" data-wow-delay=".3s">
                                <h2>Gioca Gratis</h2>
                                <p>Il tuo centro sportivo preferito non fa parte dellanostra community? Presentacelo! Per te fino a 5 partite gratis per ognistruttura che aderisce
                                </p>
                                <a href="#">Scopri</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row section2">
            <div class="container">
                <div class="prenotare">
                    <h1>Prenotare un campo non e mai stato cosi</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </p>
                </div>
            </div>
        </div>
        <!-- ====ROW-SECTION2-END==== -->
        <div class="row section3">
            <div class="container">
                <div class="badge-area">
                    <div class="col-md-4">
                        <div class="badge_bck">
                        </div>
                        <div class="badge_image">
                            <img class="wow tada " data-wow-duration="1s" data-wow-delay=".3s" src="<?php echo base_url(); ?>webroot/front/assets/images/icon1.png">
                        </div>
                        <div class="wow fadeInDown badeg_content" data-wow-duration="1s" data-wow-delay=".3s">
                            <h3>Facile da usare</h3>
                            <p> L’interfaccia semplice eintuitiva ti permetterà diprenotare la tua partita
                                senza più dover telefonare icentrisportivi della tua zona
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="badge_bck">
                        </div>
                        <div class="badge_image">
                            <img class="wow tada" data-wow-duration="1s" data-wow-delay=".3s" src="<?php echo base_url(); ?>webroot/front/assets/images/icon2.png">
                        </div>
                        <div class="wow fadeInDown badeg_content" data-wow-duration="1s" data-wow-delay=".3s">
                            <h3>Veloce</h3>
                            <p> iTrova velocemente uncampo nella tua zona e prenotalo con un click</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="badge_bck">
                        </div>
                        <div class="badge_image">
                            <img class="wow tada third_badge" data-wow-duration="1s" data-wow-delay=".3s" src="<?php echo base_url(); ?>webroot/front/assets/images/icon3.png">
                        </div>
                        <div class="wow fadeInDown badeg_content" data-wow-duration="1s" data-wow-delay=".3s">
                            <h3>Risparmia</h3>
                            <p> Ogni giorno numerosepromozioni e sconti fino al50% sul prezzo di listino del
                                campo
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ===ROW-section3-END==== -->
        <div class="row section4">
            <div class="manage_book">
                <div class="col-md-6">
                    <div class="booking_img">
                        <img src="<?php echo base_url(); ?>webroot/front/assets/images/valleyball.png">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class=" booking_content">
                        <h1 class="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".3s" >Manage bookings for <br> your sport facility</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat.
                        </p>
                        <div class="wow bounceIn booking_btn" data-wow-duration="1s" data-wow-delay=".3s">
                            <a class="find_btn" href="#">FIND OUT MORE</a>
                            <a class="book_btn" href="#">BOOK A DEMO</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ===ROW-SECTION4-END==== -->
        <div class="row section5">
            <div class="container">
                <div class="video_area">
                    <h1 class="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".3s">Do you spend a lot on the utilities <br> of your fields</h1>
                    <iframe class="video" width="560" height="315" src="https://www.youtube.com/embed/WAZ7hvQv-84" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. <br> Lorem ipsum dolor sit amet, consectetur adipisicing
                    </p>
                    <div class="wow bounceIn booking_btn_2" data-wow-duration="1s" data-wow-delay=".3s">
                        <a class="find_btn" href="#">FIND OUT MORE</a>
                        <a class="book_btn" href="#">BOOK A DEMO</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- ====ROW-SECTION5-END===== -->
        <div class="row section6">
            <div class="sport_center">
                <div class="col-md-6">
                    <div class="heading_area">
                        <h1>DO YOU MANAGE A <br> SPORTS CENTER</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet
                        </p>
                        <div class="wow bounceInLeft heading_btn" data-wow-duration="1s" data-wow-delay=".3s">
                            <a class="book_btn" href="#">BOOK A DEMO</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="section6_img">
                        <img src="<?php echo base_url(); ?>webroot/front/assets/images/capture.png">
                    </div>
                </div>
            </div>
        </div>