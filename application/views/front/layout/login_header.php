<!DOCTYPE html>
<html>
    <head>
        <title>Sport Space</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>webroot/front/css/style.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>webroot/front/css/animate.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="<?php echo base_url(); ?>webroot/front/js/toastr/toastr.min.css" rel="stylesheet" />
        <script type="text/javascript" src="<?php echo base_url(); ?>webroot/front/js/toastr/toastr.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="wow fadeInDown header" data-wow-duration="1s" data-wow-delay=".3s">
                    <div class="col-md-2">
                        <div class="logo_area">
                            <a href="<?= base_url(); ?>"><img src="<?php echo base_url(); ?>webroot/front/assets/images/logo.png"></a>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="menu_links" >
                            <div class="topnav">
                                <div id="myLinks">
                                    <li><a href="#" >I Nostri Affiliati</a></li>
                                    <li><a href="#">Come funziona</a></li>
                                    <li><a href="#">Contatti</a></li>
                                    <li><a class="blue_link" href="#">Gestisci un centro sportivo?</a></li>
                                </div>
                                <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                                <i class="fa fa-bars"></i>
                                </a>
                            </div>
                            <script>
                                function myFunction() {
                                  var x = document.getElementById("myLinks");
                                  if (x.style.display === "block") {
                                    x.style.display = "none";
                                  } else {
                                    x.style.display = "block";
                                  }
                                }
                            </script>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="login_area">
                            <ul>
                                <li><?php echo $user_name; ?></li>
                                <li><a href="<?php echo base_url(); ?>home/logout">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>