<!-- ================================FOOTER======================================     -->
        <div class="section9">
            <div class="footer">
                <div class="container">
                    <div class="row newsletter">
                        <div class="col-md-8">
                            <div class="footer_form">
                                <label for="fname">NEWSLETTER</label><br>
								<input type="email" id="newsletter" name="newsletter" placeholder="Email address" onkeyup="errorremove('newsletter_err')">
								<br>
                            	<span class="footer_submit" onClick="newsletters();">Submit</span><br>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="footer_social">
                                <h3>WE ARE SOCIAL</h3>
                                <ul>
                                    <li><a href="#"><img src="<?php echo base_url(); ?>webroot/front/assets/images/icon1-fb.png"></a></li>
                                    <li><a href="#"><img src="<?php echo base_url(); ?>webroot/front/assets/images/Instagram.png"></a></li>
                                    <li><a href="#"><img src="<?php echo base_url(); ?>webroot/front/assets/images/Twitter.png"></a></li>
                                    <li><a href="#"><img src="<?php echo base_url(); ?>webroot/front/assets/images/Pinterest.png"></a></li>
                                    <li><a href="#"><img src="<?php echo base_url(); ?>webroot/front/assets/images/Youtube.png"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row section10">
                    <div class="container">
                        <div class="row footer_menu">
                            <div class="lower_footer">
                                <div class="col-md-3">
                                    <div class="footer_logo">
                                        <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>webroot/front/assets/images/logo.png"></a>
                                        <p>Copyright @ 2021. All Rights Reserved</p>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="footer_links">
                                        <h3>ABOUT</h3>
                                        <li><a href="<?php echo base_url(); ?>partner">Partner</a></li>
                                        <li><a href="<?php echo base_url(); ?>our_place">Our Places</a></li>
                                        <li><a href="<?php echo base_url(); ?>our_vision">Our Vision</a></li>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="footer_links">
                                        <h3>INFORMATION</h3>
                                        <!-- <li><a href="#">Blog</a></li> -->
                                        <li><a href="<?php echo base_url(); ?>FAQ">FAQ'S</a></li>
                                        <li><a href="<?php echo base_url(); ?>contact">Contact</a></li>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="footer_links">
                                        <h3>TERMS & CONDITION</h3>
                                        <li><a href="<?php echo base_url(); ?>privacy_policy">Privacy Policy</a></li>
                                        <li><a href="<?php echo base_url(); ?>cookie_policy">Cookie Policy</a></li>
                                        <li><a href="<?php echo base_url(); ?>terms_conditions">Terms & Conditions</a></li>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="footer_links">
                                        <h3>CONTACT US</h3>
                                        <li><a href="#">Support@xyz.com</a></li>
                                        <li><a href="#">+1 247875123241</a></li>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ======ROW-SECTION10-END========== -->         
                <script src="<?php echo base_url(); ?>webroot/front/js/wow.min.js"></script>
                <script>
                    var wow = new WOW({
                      boxClass: 'wow', // animated element css class (default is wow)
                      animateClass: 'animated', // animation css class (default is animated)
                      offset: 0, // distance to the element when triggering the animation (default is 0)
                      mobile: true, // trigger animations on mobile devices (default is true)
                      live: true, // act on asynchronously loaded content (default is true)
                      callback: function(box) {
                        // the callback is fired every time an animation is started
                        // the argument that is passed in is the DOM node being animated
                      },
                      scrollContainer: null, // optional scroll container selector, otherwise use window,
                      resetAnimation: true, // reset animation on end (default is true)
                    });
                    wow.init();
                </script>
				<script type="text/javascript">					
		            function ValidateEmail(mail) 
		            {
		                if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
		                {
		                    return (true)
		                }
		                return (false)
		            }
		            
		            function newsletters()
		            {
		                var email = $('#newsletter').val();
		                if(email != '')
		                {
		                    var validemal = ValidateEmail(email);
		                    if(validemal)
		                    {
		                        var str = "email="+email+"&<?php echo $this->security->get_csrf_token_name(); ?>="+"<?php echo $this->security->get_csrf_hash(); ?>";
		                        $.ajax({
		                            url: '<?= base_url()?>home/newsletter',
		                            type: 'POST',
		                            data: str,
		                            dataType: 'json',
		                            cache: false,
		                            success: function(resp){
		                                if(resp.status == 'success')
		                                {
		                                    toastr.success('','Thank You For Subscribing!');
		                                    $('#newsletter').val('');
		                                }
		                                else
		                                {
		                                    toastr.error(resp.msg,'Error!');
		                                    $('#newsletter').val('');
		                                }
		                            }
		                        });
		                    }
		                    else
		                    {
		                        $('#newsletter').val('');
		                        toastr.error('Invalid email, try again','Error!');
		                    }
		                }
		                else
		                {
		                    toastr.error('Email field is required','Error!');
		                    $('#newsletter_err').text('Email field is required');
		                }
		            }
		            
		            function errorremove(id)
		            {
		                $('#'+id).text('');
		            }

		            /* Success message  */
		            function successmsg(msg)
		            {
		                toastr.success('',msg);
		            }
		            
		            function errormsg(msg)
		            {
		                toastr.error('',msg);
		            }

                    $("#msg_div").fadeOut(10000);  
                </script> 
            </div>
        </div>
        <!-- ======ROW-SECTION9-END========== -->
    </body>
</html>