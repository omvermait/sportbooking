<div class="login_section">
    <div class="container">
        <div class="row">
            <div class="register">
                <div class="login_regstr">
                    <div class="register_part">
                        <div class="login_pg_login">
                            <h1 class="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".3s">Registrati</h1>
                            <form action="">
                                <label for="customer_name"></label><br>
                                <input type="text" id="customer_name" name="customer_name" placeholder="Name" onkeyup="errorremove('err_customer_name')">
                                <span  class="text-danger" id="err_customer_name"></span>

                                <label for="login_name"></label><br>
                                <input type="text" id="login_name" name="login_name" placeholder="Username" onkeyup="errorremove('err_login_name');checkEmailUsername(this.value, 'Username')">
                                <span id="err_login_name" class="text-danger"></span>

                                <label for="customer_password"></label><br>
                                <input type="password" id="customer_password" name="customer_password" placeholder="Password" onkeyup="errorremove('err_customer_password')">
                                <span id="err_customer_password" class="text-danger"></span>

                                <label for="customer_email"></label><br>
                                <input type="email" id="customer_email" name="customer_email" placeholder="Email" onkeyup="errorremove('err_customer_email');checkEmailUsername(this.value, 'Email')">
                                <span id="err_customer_email" class="text-danger"></span>

                                <label for="customer_address"></label><br>
                                <input type="text" id="customer_address" name="customer_address" placeholder="Address" onkeyup="errorremove('err_customer_address')">
                                <span id="err_customer_address" class="text-danger"></span>

                                <label for="customer_postal_code"></label><br>
                                <input type="text" id="customer_postal_code" name="customer_postal_code" placeholder="Postcode" onkeyup="errorremove('err_customer_postal_code')">
                                <span id="err_customer_postal_code" class="text-danger"></span>

                                <label for="customer_city"></label><br>
                                <input type="text" id="customer_city" name="customer_city" placeholder="City" onkeyup="errorremove('err_customer_city')">
                                <span id="err_customer_city" class="text-danger"></span>

                                <div class="check_btn">
                                    <input type="checkbox" id="terms_conditions" name="terms_conditions" value="Bike"  onclick="errorremove('err_terms_conditions')"><label for="terms_conditions">Cliccando il tasto «Registrati» dichiari di aver letto e accettato la nostra Privacy Policy e le Condizioni di utilizz</label>
                                	<span id="err_terms_conditions"></span>
                                </div>
                            </form>
                            <a class="wow bounceIn click_here_reg" data-wow-duration="1s" data-wow-delay=".3s" href="javascript:void(0);" onclick="signup();">Registrati</a>
                            <p>Hai dimenticato la password?</p>
                            <a href="#" id="myBtn">Clicca qui per recuperarla</a>
                            <!-- The Modal -->
                            <div id="myModal" class="modal">
                                <!-- Modal content -->
                                <div class="modal-content">
                                    <span class="close">&times;</span>
                                    <div class="reset_pass">
                                        <h3>Reset Your Password</h3>
                                        <p>Please Enter Your Registered Email</p>
                                        <form>
                                            <label for="reset"></label><br>
                                            <input type="text" name="reset" id="reset_pas" placeholder="Enter Your Email Address">
                                        </form>
                                        <a href="#">Submit</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">	    
    function signup(){
        var customer_name = $('#customer_name').val();
        var login_name = $('#login_name').val();
        var customer_password = $('#customer_password').val();
        var customer_email = $('#customer_email').val();
        var customer_address = $('#customer_address').val();
        var customer_postal_code = $('#customer_postal_code').val();
        var customer_city = $('#customer_city').val();
        var terms_conditions = ($("input:checkbox[name='terms_conditions']").is(":checked")) ? '1' : '0';
        if((customer_name != '') && (login_name != '') && (customer_password != '') && (customer_email != '') && (customer_address != '') && (customer_postal_code != '') && (customer_city != '') && ($("input:checkbox[name='terms_conditions']").is(":checked"))){        	
	    	var str = "customer_name="+customer_name+"&login_name="+login_name+"&customer_password="+customer_password+"&customer_email="+customer_email+"&customer_address="+customer_address+"&customer_postal_code="+customer_postal_code+"&customer_city="+customer_city+"&terms_conditions="+terms_conditions+"&<?php echo $this->security->get_csrf_token_name(); ?>="+"<?php echo $this->security->get_csrf_hash(); ?>";
	        $.ajax({
	            url: '<?= base_url()?>home/signupData',
	            type: 'POST',
	            data: str,
	            dataType: 'json',
	            cache: false,
	            success: function(resp){
	                if(resp.status == 'success'){
	                    toastr.success('','Your Registration successfully!');
	                    $('#customer_name').val('');
	                    $('#login_name').val('');
	                    $('#customer_password').val('');
	                    $('#customer_email').val('');
	                    $('#customer_address').val('');
	                    $('#customer_postal_code').val('');
	                    $('#customer_city').val('');
	                    $('#terms_conditions').removeAttr('checked');
	                }
	                else{
	                    toastr.error(resp.msg,'Error!');	                    
	                    $('#customer_name').val('');
	                    $('#login_name').val('');
	                    $('#customer_password').val('');
	                    $('#customer_email').val('');
	                    $('#customer_address').val('');
	                    $('#customer_postal_code').val('');
	                    $('#customer_city').val('');
	                    $('#terms_conditions').removeAttr('checked');
	                }
	            }
	        });
        }
        else{
        	if(customer_name == ''){
				$('#err_customer_name').html('This Field required');
        	}
        	if(login_name == ''){
				$('#err_login_name').html('This Field required');        		
        	}
        	if(customer_password == ''){
				$('#err_customer_password').html('This Field required');        		
        	}
        	if(customer_email == ''){
				$('#err_customer_email').html('This Field required');        		
        	}
        	if(customer_address == ''){
				$('#err_customer_address').html('This Field required');        		
        	}
        	if(customer_postal_code == ''){
				$('#err_customer_postal_code').html('This Field required');        		
        	}
        	if(customer_city == ''){
				$('#err_customer_city').html('This Field required');        		
        	}
        	if(!$("input:checkbox[name='terms_conditions']").is(":checked")){
        		$('#err_customer_city').html('This Field required'); 
        	}
        }
    }
    function checkEmailUsername(f_value, f_type){
        if(f_value != ''){           
            var str = "f_value="+f_value+"&f_type="+f_type+"&<?php echo $this->security->get_csrf_token_name(); ?>="+"<?php echo $this->security->get_csrf_hash(); ?>";
            $.ajax({
                url: '<?= base_url()?>home/checkEmailUsername',
                type: 'POST',
                data: str,
                dataType: 'json',
                cache: false,
                success: function(resp){
                    if(resp.status == 'success'){
                    }
                    else{
                        toastr.error(resp.msg,'Error!'); 
                        if(f_value == 'Email'){
                            $('#customer_email').val('');    
                            $('#err_customer_email').html('This email already exist');    
                            return false;
                        }
                        else{
                            $('#login_name').val('');    
                            $('#err_login_name').html('This username already exist');    
                            return false;
                        }
                    }
                }
            });
        }
        else{
            if(f_value == 'Email'){   
                $('#err_customer_email').html('This Field required');    
                return false;
            }
            else{
                $('#err_login_name').html('This Field required');    
                return false;
            }
        }
    }
</script>
<script type="text/javascript">
    // Get the modal
    var modal = document.getElementById("myModal");
    
    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");
    
    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];
    
    // When the user clicks the button, open the modal 
    btn.onclick = function() {
      modal.style.display = "block";
    }
    
    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
      modal.style.display = "none";
    }
    
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
      if (event.target == modal) {
        modal.style.display = "none";
      }
    }
</script>