<div class="nostri_search">
    <div class="container">
        <div class="nostri_search_area">
            <div class="row">
                <div class="banner_search_content">
                    <div class="col-md-4">
                        <div class="nostri_location">
                            <label for="birthday"><img src="<?php echo base_url(); ?>webroot/front/assets/images/icon1-location.png"></label>
                            <input type="text" id="birthday" name="birthday"
                                placeholder="Citta, Indirizzo, centro Sport">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="nostri_location">
                            <label for="birthday"><img src="<?php echo base_url(); ?>webroot/front/assets/images/icon-globe.png"></label>
                            <select name="game_id" id="h_game_id">
                                <option value="">-- Select --</option>
                                <?php
                                    $game_res = $this->common_model->getData('tbl_game', array('game_status'=>'1'), 'multi');
                                    if(!empty($game_res)){
                                        foreach($game_res as $g_val) {
                                            ?>
                                            <option value="<?php echo $g_val->game_id; ?>"><?php echo $g_val->game_name; ?></option>
                                            <?php
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="nostri_calender">
                            <label for="birthday"><img src="<?php echo base_url(); ?>webroot/front/assets/images/icon-calendar.png"></label>
                            <input type="date" id="birthday" name="birthday">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="nostri_btn">
                            <a href="#">FIND SPACE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="nostri_main_section">
    <div class="container">
        <div class="row">
            <div class="main_section">
                <div class="col-md-3">
                    <div class="wow fadeInLeft main_section_sidebar"data-wow-duration="1s" data-wow-delay=".3s">
                        <h2>Cambia Sport</h2>
                        <label for="birthday"></label>
                        <select name="game_id" id="s_game_id" onchange="callAjaxGroundList(9, 0);">
                            <option value="">-- Select --</option>
                            <?php
                                $game_res = $this->common_model->getData('tbl_game', array('game_status'=>'1'), 'multi');
                                if(!empty($game_res)){
                                    foreach($game_res as $g_val) {
                                        ?>
                                        <option value="<?php echo $g_val->game_id; ?>"><?php echo $g_val->game_name; ?></option>
                                        <?php
                                    }
                                }
                            ?>
                        </select>
                        <h2>Ordina per:</h2>
                        <h5>Cost of the pitch</h5>
                        <input type="range" min="1" max="100" value="0" class="slider" id="myRange">
                        <p><span id="demo"></span></p>
                        <h5>Distanza</h5>
                        <input type="range" min="1" max="1000" value="0" class="slider" id="myRange-two">
                        <p><span id="demo-two"></span></p>
                        <div class="sports_area">
                            <h3>Team</h3>
                            <select name="play_members_id" id="play_members_id" onchange="callAjaxGroundList(9, 0);">
                                <option value="">-- Select --</option>
                                <?php
                                    $play_members = $this->common_model->getData('tbl_play_members', array('play_members_status'=>'1'), 'multi');
                                    if(!empty($play_members)){
                                        foreach($play_members as $pm_val) {
                                            ?>
                                            <option value="<?php echo $pm_val->play_members_id; ?>"><?php echo $pm_val->play_members; ?></option>
                                            <?php
                                        }
                                    }
                                ?>
                            </select>
                            <h3>Services</h3>
                            <select name="services_id" id="services_id" onchange="callAjaxGroundList(9, 0);">
                                <option value="">-- Select --</option>
                                <?php
                                    $services_res = $this->common_model->getData('tbl_services', array('services_status'=>'1'), 'multi');
                                    if(!empty($services_res)){
                                        foreach($services_res as $s_val) {
                                            ?>
                                            <option value="<?php echo $s_val->services_id; ?>"><?php echo $s_val->services_name; ?></option>
                                            <?php
                                        }
                                    }
                                ?>
                            </select>
                        </div><!-- 
                        <div class="other_provinces">
                            <h3>Altre Regioni</h3>
                            <ul>
                                <li><a href="#">Abruzzo</a></li>
                                <li><a href="#">Basilicata</a></li>
                                <li><a href="#">Calabria</a></li>
                                <li><a href="#">Campania</a></li>
                                <li><a href="#">Emilia-Romagna</a></li>
                                <li><a href="#">Friuli Venezia-Giulia</a></li>
                                <li><a href="#">Lazio</a></li>
                                <li><a href="#">Liguria</a></li>
                                <li><a href="#">Lombardia</a></li>
                                <li><a href="#">Marche</a></li>
                                <li><a href="#">Molise</a></li>
                                <li><a href="#">Piemonte</a></li>
                                <li><a href="#">Puglia</a></li>
                                <li><a href="#">Sardegna</a></li>
                                <li><a href="#">Sicilia</a></li>
                                <li><a href="#">Toscana</a></li>
                            </ul>
                        </div> -->
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="nostri_content">
                        <div class="nostri_content_heading">
                            <h1 class="wow fadeInDown "data-wow-duration="1s" data-wow-delay=".3s">CENTRI SPORTIVI</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodoconsequat. Duis aute irure dolor in reprehenderit in voluptate velit </p>
                        </div>
                        <div class="sports_center" id="ground_details">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var slider = document.getElementById("myRange");
    var output = document.getElementById("demo");
    output.innerHTML = slider.value;
    
    slider.oninput = function() {
      output.innerHTML = this.value;
    }
    
    var slider1 = document.getElementById("myRange-two");
    var output1 = document.getElementById("demo-two");
    output1.innerHTML = slider1.value;
    
    slider1.oninput = function() {
      output1.innerHTML = this.value;
    }
</script>
<script type="text/javascript">
    var limit = '9';
    function callAjaxGroundList(limit, offset) 
    {
        var game_id = $('#s_game_id').val();
        var play_members_id = $('#play_members_id').val();
        var services_id = $('#services_id').val();
        var PAGE = '<?php echo base_url(); ?>home/groundList';
        var limit = limit;
        var offset =offset;
        jQuery.ajax({
            type :"POST",
            url  :PAGE,
            data :{limit:limit, offset:offset, game_id:game_id, play_members_id:play_members_id, services_id:services_id},
            success:function(response)
            {    
                jQuery('#ground_details').html(response);
                jQuery(".g_pagination").click(function(){
                    var offset =jQuery(this).data("id");
                    callAjaxGroundList(limit, offset);
                });
            } 
        });
    }
    callAjaxGroundList(limit,0);
</script>
