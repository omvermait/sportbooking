<div class="detail_section1">
    <div class="container">
        <div class="row">
            <div class="detail_featured_img">
                <h1>PRENOTO ONLINE IL TUO<br>SPORT PREFERITO</h1>
            </div>
        </div>
    </div>
</div>
<!-- =====Header-SECTION1-END==== -->
<div class="detail_section2">
    <div class="container">
        <div class="row">
            <div class="detail_section2_content nostri_btn">
                <a href="<?php echo base_url(); ?>home/our_place"><i class="fas fa-arrow-left"></i>Back</a>
            </div>
        </div>
    </div>
</div>
<div class="detail_section3">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <div class="detail_section2_img">
                    <img src="<?php echo base_url().$ground_data->ground_img; ?>" width="150" height="150">
                </div>
            </div>
            <div class="col-md-10">
                <div class="detail_section2_content">
                    <h2><?php echo $ground_data->ground_name; ?></h2>
                    <P><i class="fas fa-map-marker-alt"></i><?php echo $ground_data->ground_address; ?> </P>
                    <div class="line_two">
                        <p><i class="far fa-heart"></i>234 Segui</p>
                        <p><i class="fas fa-star"></i>3.5</p>
                        <p>29 Recentioni</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- =====detail_section3-END==== -->
<div class="detail_slider">
    <div class="container">
        <div class="row">
            <div class="detail_slider">
                <div class="owl-slider">
                    <div id="carousel" class="owl-carousel">
                        <?php
                            $ground_image = $this->common_model->getData('tbl_ground_img', array('ground_id'=>$ground_data->ground_id, 'ground_img_status'=>'1'), 'multi');
                            if(!empty($ground_image)){
                                foreach ($ground_image as $g_img) {
                                    ?>
                                    <div class="item">
                                        <img class="owl-lazy" data-src="<?php echo base_url().$g_img->ground_img; ?>" alt="">
                                    </div>
                                    <?php
                                }
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- =====detail_slider-END==== -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js'></script>
<script>
    jQuery("#carousel").owlCarousel({
        loop:true,
        autoplay: true,
        lazyLoad: true,
        rewind: false,
        margin: 20,
        /*
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        */
        responsiveClass: true,
        autoHeight: true,
        autoplayTimeout: 7000,
        smartSpeed: 800,
        nav: true,
        responsive: {
            0: {
                items: 1
            },            
            600: {
                items: 3
            },            
            1024: {
                items: 7
            },            
            1366: {
                items: 7
            }
        }
    });
</script>
<div class="detail_section4">
    <div class="container">
        <div class="row">
            <div class="servizi_area">
                <div class="col-md-3">
                    <div class="servizi">
                        <h1>Servizi</h1>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="servizi_content">
                        <div class="row">
                            <?php
                                $service_arr = explode(',', $ground_data->services_id);
                                if(!empty($service_arr)){
                                    foreach($service_arr as $s_val) {
                                        $service_res = $this->common_model->getData('tbl_services', array('services_id'=>$s_val), 'single');
                                        if(!empty($service_res)){
                                            ?>
                                            <div class="col-md-3">
                                                <div class="servizi_col">
                                                    <ul>
                                                        <li><img width="20" height="20" src="<?php echo base_url().$service_res->services_img; ?>">&nbsp;<?php echo $service_res->services_name; ?></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <?php
                                        }

                                    }
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- =====Header-SECTION4-END==== -->
<div class="detail_section5">
    <div class="container">
        <div class="row">
            <div class="detail_scroller">
                <h2><?php echo $ground_data->ground_name; ?></h2>
                <p><?php echo $ground_data->ground_description; ?></p>
            </div>
        </div>
    </div>
</div>
<!-- =====Header-SECTION5-END==== -->
<div class="detail_section6">
    <div class="container">
        <div class="row">
            <div class="nav_btn">
                <div class="col-md-4">
                    <div class="left_nav_btn">
                        <a href="#"><i class="fas fa-arrow-left"></i></a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="nav_calender">
                        <label for="birthday"></label>
                        <input type="date" id="birthday" name="birthday">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="left_nav_btn">
                        <a href="#"><i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="detail_thumbnail_slider">
    <div class="container">
        <div class="row">
            <div class="detail_thumbnail_slider_area">
                <div class="owl-slider">
                    <div id="carousel01" class="owl-carousel">
                        <?php
                            $game_res = $this->common_model->getData('tbl_game', array('game_status'=>'1'), 'multi');
                            if(!empty($game_res)){
                                foreach($game_res as $g_val) {
                                    ?>
                                    <div class="item1">
                                        <a href="">
                                            <div class="detail_slide_content">
                                                <img src="<?php echo base_url().$g_val->game_img; ?>" width="50" height="50">
                                                <h3><?php echo $g_val->game_name; ?></h3>
                                            </div>
                                        </a>
                                    </div>
                                    <?php
                                }
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- =====detail_thumbnail_slider-END==== -->
<script>
    jQuery("#carousel01").owlCarousel({
        loop:true,
        autoplay: false,
        lazyLoad: true,
        rewind: false,
        margin: 20,
        /*
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        */
        responsiveClass: true,
        autoHeight: true,
        autoplayTimeout: 7000,
        smartSpeed: 800,
        nav: true,
        responsive: {
            0: {
                items: 1
            },            
            600: {
                items: 3
            },            
            1024: {
                items: 4
            },            
            1366: {
                items: 4
            }
        }
    });
</script>
<div class="detail_section7">
    <div class="container">
        <div class="row">
            <div class="detail_table">
                <div class="col-md-3">
                    <div class="detail_time">
                        <ul>
                            <?php
                                $game_time_slot = $this->common_model->getData('tbl_ground_time_slot', array('ground_id'=>$ground_data->ground_id, 'ground_time_slot_status'=>'1'), 'multi');
                                if(!empty($game_time_slot)){
                                    foreach ($game_time_slot as $gt_val) {
                                        ?>
                                        <li><?php echo $gt_val->start_time.' - '.$gt_val->end_time; ?></li>
                                        <?php
                                    }
                                }
                            ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="detail_days">
                        <div class="detail_days_heading">
                            <div class="heading_box">
                                <h4>Mon</h4>
                            </div>
                            <div class="heading_box">
                                <h4>Tue</h4>
                            </div>
                            <div class="heading_box">
                                <h4>Wed</h4>
                            </div>
                            <div class="heading_box">
                                <h4>Thu</h4>
                            </div>
                            <div class="heading_box">
                                <h4>Fri</h4>
                            </div>
                            <div class="heading_box">
                                <h4>Sat</h4>
                            </div>
                            <div class="heading_box">
                                <h4>Sun</h4>
                            </div>
                        </div>
                        <?php
                            $game_time_slot = $this->common_model->getData('tbl_ground_time_slot', array('ground_id'=>$ground_data->ground_id, 'ground_time_slot_status'=>'1'), 'multi');
                            if(!empty($game_time_slot)){
                                foreach ($game_time_slot as $gt_val) {
                                    ?>
                                    <div class="detail_days_box">
                                        <div class="box"></div>
                                        <div class="box"></div>
                                        <div class="box"></div>
                                        <div class="box"></div>
                                        <div class="box"></div>
                                        <div class="box"></div>
                                        <div class="box"></div>
                                    </div>
                                    <?php
                                }
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <div class="detail_section7">
    <div class="container">
        <div class="row">
            <div class="campi_area">
                <h1>Campi</h1>
                <div class="sports_center">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="detail_box">
                                <div class="hover_img">
                                    <a href="detail.html"><img src="<?php echo base_url(); ?>webroot/front/assets/images/ground.jpg">
                                    </a>
                                    <a class="ground_link" href="#"> DELHI GROUND</a>
                                    <a href="#" class="overlay">BOOK NOW</a>
                                </div>
                                <div class="hover_img_details">
                                    <h3>Campo H1</h3>
                                    <p>Erba SinteticaScoperto</p>
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <div class="hover_img_details_btn"> 
                                        <a href="#">INSTANT BOOK</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="detail_box">
                                <div class="hover_img">
                                    <a href="detail.html"><img src="<?php echo base_url(); ?>webroot/front/assets/images/ground.jpg">
                                    </a>
                                    <a class="ground_link" href="#"> DELHI GROUND</a>
                                    <a href="#" class="overlay">BOOK NOW</a>
                                </div>
                                <div class="hover_img_details">
                                    <h3>Campo H1</h3>
                                    <p>Erba SinteticaScoperto</p>
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <div class="hover_img_details_btn"> 
                                        <a href="#">INSTANT BOOK</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="detail_box">
                                <div class="hover_img">
                                    <a href="detail.html"><img src="<?php echo base_url(); ?>webroot/front/assets/images/ground.jpg">
                                    </a>
                                    <a class="ground_link" href="#"> DELHI GROUND</a>
                                    <a href="#" class="overlay">BOOK NOW</a>
                                </div>
                                <div class="hover_img_details">
                                    <h3>Campo H1</h3>
                                    <p>Erba SinteticaScoperto</p>
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <div class="hover_img_details_btn"> 
                                        <a href="#">INSTANT BOOK</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="detail_box">
                                <div class="hover_img">
                                    <a href="detail.html"><img src="<?php echo base_url(); ?>webroot/front/assets/images/ground.jpg">
                                    </a>
                                    <a class="ground_link" href="#"> DELHI GROUND</a>
                                    <a href="#" class="overlay">BOOK NOW</a>
                                </div>
                                <div class="hover_img_details">
                                    <h3>Campo H1</h3>
                                    <p>Erba SinteticaScoperto</p>
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <div class="hover_img_details_btn"> 
                                        <a href="#">INSTANT BOOK</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="detail_box">
                                <div class="hover_img">
                                    <a href="detail.html"><img src="<?php echo base_url(); ?>webroot/front/assets/images/ground.jpg">
                                    </a>
                                    <a class="ground_link" href="#"> DELHI GROUND</a>
                                    <a href="#" class="overlay">BOOK NOW</a>
                                </div>
                                <div class="hover_img_details">
                                    <h3>Campo H1</h3>
                                    <p>Erba SinteticaScoperto</p>
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <div class="hover_img_details_btn"> 
                                        <a href="#">INSTANT BOOK</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="detail_box">
                                <div class="hover_img">
                                    <a href="detail.html"><img src="<?php echo base_url(); ?>webroot/front/assets/images/ground.jpg">
                                    </a>
                                    <a class="ground_link" href="#"> DELHI GROUND</a>
                                    <a href="#" class="overlay">BOOK NOW</a>
                                </div>
                                <div class="hover_img_details">
                                    <h3>Campo H1</h3>
                                    <p>Erba SinteticaScoperto</p>
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                                    <div class="hover_img_details_btn"> 
                                        <a href="#">INSTANT BOOK</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->