<div class="row">
    <?php
        if(!empty($ground_data)){
            foreach ($ground_data as $g_data) {
                ?>
                <div class="col-md-4">
                    <div class="detail_box">
                        <div class="hover_img">
                            <a href="<?php echo base_url(); ?>home/gameDetail/<?php echo $g_data->ground_id; ?>"><img src="<?php echo base_url().$g_data->ground_img; ?>">
                            </a>
                            <a class="ground_link" href="<?php echo base_url(); ?>home/gameDetail/<?php echo $g_data->ground_id; ?>""> <?php echo $g_data->ground_address; ?></a>
                            <!-- <a href="#" class="overlay">BOOK NOW</a> -->
                        </div>
                        <div class="hover_img_details">
                            <h3><?php echo $g_data->ground_name; ?></h3>
                            <!-- <p>Erba SinteticaScoperto</p> -->
                            <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                            <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                            <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                            <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                            <img src="<?php echo base_url(); ?>webroot/front/assets/images/star.png">
                            <div class="hover_img_details_btn"> 
                                <a href="#">INSTANT BOOK</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
        else{
            ?>
            <div class="col-md-12">
                <h3>No Records Founds...</h3>
            </div>
            <?php
        }
?>
</div>
<br/>
<div class="row">
<?php
        if($total_count != '0'){
            $currentPage =$offset; 
            $lastPage = ceil($total_count/$limit);
            $firstPage = 1;
            $nextPage = $currentPage + 1; 
            $previousPage = $currentPage - 1; 
            ?>
            <ul>
                <?php
                    if($currentPage >= 1)
                    { ?>
                        <li class="nostri_btn">
                            <a data-id="<?php echo $previousPage; ?>" class="btn-sm g_pagination" aria-label="Previous">
                                <!-- <span aria-hidden="true">&laquo;</span> -->
                                <i class="fas fa-angle-double-left"></i>
                            </a>
                        </li>
                        <?php
                    }
                    $j=1;
                    for($pn=$currentPage; $pn<$lastPage; $pn++)
                    {   
                        if($j<=3)
                        {
                            if($pn == $currentPage)
                            {
                               $page= $pn+1;
                                ?>
                                <li class="nostri_btn"><a data-id="<?php echo $pn; ?>" class="btn-sm g_pagination"><?php echo $page; ?></a></li>
                                <?php 
                            }
                            else
                            {
                               $page= $pn+1;
                                ?>
                                <li class="nostri_btn"><a data-id="<?php echo $pn; ?>" class="btn-sm g_pagination"><?php echo $page; ?></a></li>
                                <?php 
                            }
                            $j++;
                        }
                    }
                    if($nextPage != $lastPage)
                    { 
                        ?>
                        <li class="nostri_btn">
                            <a data-id="<?php echo $nextPage; ?>" class="btn-sm g_pagination" aria-label="Next">
                                <!-- <span aria-hidden="true">&raquo;</span> -->
                                <i class="fas fa-angle-double-right"></i>
                            </a>
                        </li>
                        <?php
                    }
                ?>
            </ul>
            <?php
        }
    ?>
</div>