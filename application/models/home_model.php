<?php
class Home_model extends CI_Model {

	function __construct()
	{
       parent::__construct();
	   $this->load->database();
	}	

	function checkUserLogin($data)
	{
		$login_name = $data['login_name'];		
		$customer_password = $data['customer_password'];		
        $this->db->select('*');
		$this->db->from('tbl_customer');	
		$this->db->where("((login_name = '".$login_name."') OR (customer_email = '".$login_name."'))");
		$this->db->where('customer_password',$customer_password);
		$this->db->where('customer_status','1');
		$query = $this->db->get();		
		return $query->result();
	}

	public function CountGroundList($game_id,$play_members_id,$services_id)
	{
		$this->db->select('a.*');
		$this->db->from('tbl_ground a');
		$this->db->join('tbl_game b', 'a.game_id=b.game_id', 'inner');
		$this->db->where('a.ground_status', '1');
		if(!empty($game_id)){
			$this->db->where('a.game_id', $game_id);
		}

		if(!empty($play_members_id)){
			$w_play_members_id = "FIND_IN_SET('".$play_members_id."', b.play_members_id)";
			$this->db->where($w_play_members_id);
		}

		if(!empty($services_id)){
			$w_services_id = "FIND_IN_SET('".$services_id."', a.services_id)";
			$this->db->where($w_services_id);
		}
		$query = $this->db->get();	
		return $query->result() ;
	} 

    public function getGroundList($limit,$offset,$game_id,$play_members_id,$services_id)
	{
		$this->db->select('a.*');
		$this->db->from('tbl_ground a');
		$this->db->join('tbl_game b', 'a.game_id=b.game_id', 'inner');
		$this->db->where('a.ground_status', '1');
		if(!empty($game_id)){
			$this->db->where('a.game_id', $game_id);
		}

		if(!empty($play_members_id)){
			$w_play_members_id = "FIND_IN_SET('".$play_members_id."', b.play_members_id)";
			$this->db->where($w_play_members_id);
		}

		if(!empty($services_id)){
			$w_services_id = "FIND_IN_SET('".$services_id."', a.services_id)";
			$this->db->where($w_services_id);
		}
		$this->db->limit($limit,$offset);
		$query = $this->db->get();	
		return $query->result() ;
	} 
}
?>